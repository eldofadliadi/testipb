const paramGetPackage = {
    "take" : 3,
    "skip" : 0,
    "keyword" : ""
};

const paketMakanan = {
    "_id" : "61237b14a0e66000114a4c6c",
    "name" : "Paket Makanan Isoman",
    "total" : 190,
    "createdAt" : "2021-08-23T10:40:20.680Z",
    "isActive" : true
};

const newPackage = {
    "name" : "jest package",
    "total" : 100
};

const updatePackage = {
    "name" : "jest update",
    "total" : 200
};

module.exports = {
    paramGetPackage, paketMakanan, newPackage, updatePackage
}