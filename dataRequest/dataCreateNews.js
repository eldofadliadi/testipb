const postNews = {
    "name": "test berita 3",
    "title": "test berita 3",
    "caption": "test berita 3",
    "newsCategoryIds": [
        "60f7dfa4245c290012ff143a"
    ],
    "description": "test berita 3",
    "route": "test-berita-3",
    "userId": "60ebed4a127d0700116f596b",
    "createAt": "2021-12-13T10:35:37.640Z",
    "updateAt": "2021-12-13T10:35:37.640Z",
    "isPinned": false
};

const postNews2 = {
    "name": "test berita 3",
    "title": "test berita 3",
    "caption": "test berita 3",
    "description": "test berita 3",
    "route": "test-berita-3",
    "userId": "60ebed4a127d0700116f596b",
    "createAt": "2021-12-13T10:35:37.640Z",
    "updateAt": "2021-12-13T10:35:37.640Z",
    "isPinned": false
};

const updateNews = {
    "name": "test update 3",
    "title": "test update 3",
    "caption": "test update 3",
    "newsCategoryIds": [
        "60f7dfa4245c290012ff143a"
    ],
    "description": "test update 3",
    "route": "test-update-3",
    "userId": "60ebed4a127d0700116f596b",
    "createAt": "2021-12-13T10:35:37.640Z",
    "updateAt": "2021-12-13T10:35:37.640Z",
    "isPinned": false
}

module.exports = {
    postNews,updateNews, postNews2
}