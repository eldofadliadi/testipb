const paramGetListEmbed = {
    "keyword" : "",
    "take" : 10,
    "skip" : 0,
    "platform" : ""
};

const createNewEmbed = {
    "platform" : 101,
    "title" : "test media embed",
    "embedUrl" : "https://www.youtube.com/embed/VqxoJdiRB5o"
};

const updateMediaEmbed = {
    "platform" : 101,
    "title" : "update embed",
    "embedUrl" : "https://www.youtube.com/embed/VqxoJdiRB5o"
};

module.exports = {
    paramGetListEmbed, createNewEmbed, updateMediaEmbed
}