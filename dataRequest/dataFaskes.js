// let idRegion;

const createRegionFaskes = {
    name : "jest test"
};

const updateRegionFaskes = {
    name : " jest update"
};

const paramRegion = {
    "take" : 10,
    "skip" : 0,
    "keyword" : "jest"
};

const paramLokasi = {
    "take" : 10,
    "skip" : 0,
    "keyword" : "jest"
};

// const createLokasiFaskes = {
//     "regionId"   : idRegion,
//     "name" : "st lokajesi",
//     "city" : "jest city",
//     "address" : "alamat jest lokasi",
//     "latitude" : "",
//     "longitude" : ""
// };

const updateLokasiFaskes = {
    "name" : "jest lokasi update",
    "city" : "jest city update",
    "address" : "alamat jest lokasi update",
    "latitude" : "",
    "longitude" : ""
};

module.exports = {
    createRegionFaskes, updateRegionFaskes, paramRegion, paramLokasi, updateLokasiFaskes
};