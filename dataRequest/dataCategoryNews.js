const paramGetCategory = {
    "take" : 3,
    "skip" : 0,
};


const idCategory = "60f7dfa4245c290012ff143a";

const createCategoryNews = {
    "name": "test kategori",
    "route": "test-kategori"
};

const updateCategoryNews = {
    "name": "test update kategori",
    "route": "test-update-kategori"
};

const paramUpdateCategory = {
    "take" : 1,
    "skip" : 0,
    "keyword": "test kategori"
};

module.exports = {
    paramGetCategory, idCategory, createCategoryNews, updateCategoryNews, paramUpdateCategory
}