let date = new Date();
let date2 = new Date(date);
date2.setDate(date2.getDate() +2);
let curDate = date.toJSON();
let tomDate = date2.toJSON();

const newWebinar = {
    "title": "jest webinar",
    "caption": "jest webinar",
    "startDate": `${curDate}`,
    "endDate": `${tomDate}`,
    "liveUrl": "www.google.com"
};

const updateWebinar = {
    "title": "jest webinar update",
    "caption": "jest webinar update",
    "liveUrl": "www.yahoo.com"
};

module.exports = {
    newWebinar,updateWebinar
};