const newOrder = {
    "orders": [
        {
          "courier": "jne",
          "courier_service": "REG",
          "order_number": "",
          "is_cod": true,
          "delivery_type": "",
          "delivery_time": "",
          "is_send_company": true,
          "origin_lat": -6.327337099999999,
          "origin_long": 106.8207875,
          "origin_subdistrict_code": "31.74.09",
          "origin_subdistrict_name": "JAGAKARSA",
          "origin_city_code": "31.74",
          "origin_city_name": "KOTA ADM. JAKARTA SELATAN",
          "origin_province_code": "31",
          "origin_province_name": "DKI JAKARTA",
          "origin_contact_name": "Shipdeo Testing 3",
          "origin_contact_phone": "0821912812918",
          "origin_contact_address": "Teks Alamat Lengkap",
          "origin_contact_email": "",
          "origin_note": "",
          "origin_postal_code": "12620",
          "destination_lat": 0,
          "destination_long": 0,
          "destination_subdistrict_code": "32.73.13",
          "destination_subdistrict_name": "Lengkong",
          "destination_city_code": "32.73",
          "destination_city_name": "KOTA BANDUNG",
          "destination_province_code": "32",
          "destination_province_name": "JAWA BARAT",
          "destination_postal_code": "40264",
          "destination_contact_name": "Kontak Test 1",
          "destination_company_name": " ",
          "destination_contact_phone": "999111222",
          "destination_contact_address": "Jalan kontak test 1",
          "destination_contact_email": "kontaktest1@getnada.com",
          "destination_note": " ",
          "delivery_note": " ",
          "reference_number": " ",
          "airwaybill_number": " ",
          "branch_id": "612607b6de6deead1542fae6",
          "dapartment_id": "612607b6de6dee3ac142fae8",
          "user_child_id": "60d3f44878b4654eca447559",
          "is_dropship": true,
          "dropship_name": "test dropship",
          "dropship_phone": "99223344",
          "items": [
            {
              "name": "test barang",
              "description": "-",
              "weight": 1,
              "weight_uom": "kg",
              "qty": 1,
              "value": 1000000,
              "width": 10,
              "height": 10,
              "length": 10,
              "dimension_uom": "cm",
              "total_value": 1000000,
              "codValue": 1020000,
              "currentCodValue": 1020000
            }
          ],
          "transaction": {
            "subtotal": 1000000,
            "shipping_charge": 11000,
            "fee_insurance": 7000,
            "is_insuranced": true,
            "discount": 0,
            "total_value": 1000000,
            "total_cod": 1020000,
            "weight": 1,
            "width": 10,
            "height": 10,
            "length": 10,
            "coolie": 1,
            "package_category": "package",
            "package_content": "test barang",
            "is_wood_packing": false,
            "fee_wood_packing": 0
          }
        }
    ]
};

const emptyDataOrder = {
  "orders": [
    {
      "courier": " ",
      "courier_service": " ",
      "order_number": " ",
      "is_cod": " ",
      "delivery_type": " ",
      "delivery_time": " ",
      "is_send_company": " ",
      "origin_lat": " ",
      "origin_long": " ",
      "origin_subdistrict_code": " ",
      "origin_subdistrict_name": " ",
      "origin_city_code": " ",
      "origin_city_name": " ",
      "origin_province_code": " ",
      "origin_province_name": " ",
      "origin_contact_name": " ",
      "origin_contact_phone": " ",
      "origin_contact_address": " ",
      "origin_contact_email": " ",
      "origin_note": " ",
      "origin_postal_code": " ",
      "destination_lat": " ",
      "destination_long": " ",
      "destination_subdistrict_code": " ",
      "destination_subdistrict_name": " ",
      "destination_city_code": " ",
      "destination_city_name": " ",
      "destination_province_code": " ",
      "destination_province_name": " ",
      "destination_postal_code": " ",
      "destination_contact_name": " ",
      "destination_company_name": " ",
      "destination_contact_phone": " ",
      "destination_contact_address": " ",
      "destination_contact_email": " ",
      "destination_note": " ",
      "delivery_note": " ",
      "reference_number": " ",
      "airwaybill_number": " ",
      "branch_id": " ",
      "dapartment_id": " ",
      "user_child_id": " ",
      "is_dropship": " ",
      "dropship_name": " ",
      "dropship_phone": " ",
      "items": [
        {
          "name": " ",
          "description": " ",
          "weight": " ",
          "weight_uom": " ",
          "qty": " ",
          "value": " ",
          "width": " ",
          "height": " ",
          "length": " ",
          "dimension_uom": " ",
          "total_value": " "
        }
      ],
      "transaction": {
        "subtotal": " ",
        "shipping_charge": " ",
        "fee_insurance": " ",
        "is_insuranced": " ",
        "discount": " ",
        "total_value": " ",
        "total_cod": " ",
        "weight": " ",
        "width": " ",
        "height": " ",
        "length": " ",
        "coolie": " ",
        "package_category": " ",
        "package_content": " ",
        "is_wood_packing": " ",
        "fee_wood_packing": " "
      }
    }
  ]
};

const failJneRegNonCod = {
  "orders": [
      {
        "courier": "jne",
        "courier_service": "REG",
        "order_number": "",
        "is_cod": false,
        "delivery_type": "",
        "delivery_time": "",
        "is_send_company": true,
        "origin_lat": -6.327337099999999,
        "origin_long": 106.8207875,
        "origin_subdistrict_code": "1.74.09",
        "origin_subdistrict_name": "JAGAKARSA",
        "origin_city_code": "31.74",
        "origin_city_name": "KOTA ADM. JAKARTA SELATAN",
        "origin_province_code": "31",
        "origin_province_name": "DKI JAKARTA",
        "origin_contact_name": "Shipdeo Testing 3",
        "origin_contact_phone": "0821912812918",
        "origin_contact_address": "Teks Alamat Lengkap",
        "origin_contact_email": "",
        "origin_note": "",
        "origin_postal_code": "12620",
        "destination_lat": 0,
        "destination_long": 0,
        "destination_subdistrict_code": "32.73.13",
        "destination_subdistrict_name": "Lengkong",
        "destination_city_code": "32.73",
        "destination_city_name": "KOTA BANDUNG",
        "destination_province_code": "32",
        "destination_province_name": "JAWA BARAT",
        "destination_postal_code": "40264",
        "destination_contact_name": "Kontak Test 1",
        "destination_company_name": " ",
        "destination_contact_phone": "999111222",
        "destination_contact_address": "Jalan kontak test 1",
        "destination_contact_email": "kontaktest1@getnada.com",
        "destination_note": " ",
        "delivery_note": " ",
        "reference_number": " ",
        "airwaybill_number": " ",
        "branch_id": "612607b6de6deead1542fae6",
        "dapartment_id": "612607b6de6dee3ac142fae8",
        "user_child_id": "60d3f44878b4654eca447559",
        "is_dropship": false,
        "dropship_name": " ",
        "dropship_phone": " ",
        "items": [
          {
            "name": "test barang",
            "description": " ",
            "weight": 2,
            "weight_uom": "kg",
            "qty": 1,
            "value": 1000000,
            "width": 0,
            "height": 0,
            "length": 0,
            "dimension_uom": "cm",
            "total_value": 1000000
          }
        ],
        "transaction": {
          "subtotal": 1000000,
          "shipping_charge": 22000,
          "fee_insurance": 0,
          "is_insuranced": false,
          "discount": 0,
          "total_value": 1000000,
          "total_cod": 0,
          "weight": 1,
          "width": 0,
          "height": 0,
          "length": 0,
          "coolie": 1,
          "package_category": "package",
          "package_content": "test barang",
          "is_wood_packing": false,
          "fee_wood_packing": 0
        }
      }
  ]
};

const paramOrderList = {
    "take": 3,
    "skip": 0
};

const updateOrder = {
    "courier": "jne",
    "courier_service": "REG",
    "is_cod": true,
    "is_dropship": true,
    "delivery_type": "pickup",
    "delivery_time": "",
    "is_send_company": false,
    "origin_lat": -6.327337099999999,
    "origin_long": 106.8207875,
    "origin_subdistrict_code": "31.74.09",
    "origin_subdistrict_name": "JAGAKARSA",
    "origin_city_code": "31.74",
    "origin_city_name": "KOTA ADM. JAKARTA SELATAN",
    "origin_province_code": "31",
    "origin_province_name": "DKI JAKARTA",
    "origin_contact_name": "PT Test Shipdeo Tiga",
    "origin_contact_phone": "0821912812918",
    "origin_contact_address": "Teks Alamat Lengkap",
    "origin_contact_email": "",
    "origin_note": "",
    "origin_postal_code": "12620",
    "destination_lat": 0,
    "destination_long": 0,
    "destination_subdistrict_code": "32.73.13",
    "destination_subdistrict_name": "Lengkong",
    "destination_city_code": "32.73",
    "destination_city_name": "KOTA BANDUNG",
    "destination_province_code": "32",
    "destination_province_name": "JAWA BARAT",
    "destination_postal_code": "40264",
    "destination_contact_name": "Kontak Test 1",
    "destination_contact_phone": "999111222",
    "destination_contact_address": "Jalan kontak test 1",
    "destination_contact_email": "kontaktest1@getnada.com",
    "destination_note": "",
    "destination_company_name": "",
    "delivery_note": "-",
    "reference_number": "",
    "airwaybill_number": "",
    "items": [
      {
        "name": "update barang",
        "description": "-",
        "weight": 2,
        "weight_uom": "kg",
        "qty": 1,
        "value": 1500000,
        "width": 10,
        "height": 10,
        "length": 10,
        "dimension_uom": "cm",
        "total_value": 1500000,
        "codValue": 1530000,
        "currentCodValue": 1530000
      }
    ],
    "transaction": {
      "subtotal": 1500000,
      "shipping_charge": 22000,
      "fee_insurance": 8000,
      "is_insuranced": true,
      "discount": 0,
      "total_value": 1500000,
      "total_cod": 1530000,
      "weight": 2,
      "width": 10,
      "height": 10,
      "length": 10,
      "coolie": 1,
      "package_category": "package",
      "package_content": "update barang",
      "is_wood_packing": false,
      "fee_wood_packing": 0
    }
};

let date = new Date();
let year = date.getFullYear();
let month = date.getMonth()+1;
let dt = date.getDate();

if (dt < 10) {
    dt = '0' + dt;
}
if (month < 10) {
    month = '0' + month;
}
let curDate = `${year}-${month}-${dt}`;

const paramOrderDashboard = {
  "startDate": `${curDate}`,
  "endDate": `${curDate}`
};

const paramCityDestination = {
  "take": 10,
  "skip": 0,
  "status": 1,
  "startDate": `${curDate}`,
  "endDate": `${curDate}`
};

module.exports = {
    newOrder, paramOrderList, updateOrder, failJneRegNonCod, emptyDataOrder, paramOrderDashboard, paramCityDestination
}