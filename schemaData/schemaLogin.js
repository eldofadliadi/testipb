const schemaLogin = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "accessToken": "8739e4cb12a27efbded38ec74379ba16b869976c",
            "accessTokenExpiresAt": "2021-12-14T07:10:36.509Z",
            "refreshToken": "c343dfe0405dda3365ce830cced13596016cc087",
            "refreshTokenExpiresAt": "2021-12-14T07:10:36.509Z",
            "client": {
                "id": "60ec2cc85f2aa900111e207f",
                "clientId": "indonesiapastibisa-internal",
                "clientSecret": "S!C3P4T",
                "grants": [
                    "password",
                    "refresh_token",
                    "authorization_code"
                ]
            },
            "user": {
                "_id": "60ebed4a127d0700116f596b",
                "username": "superadmin",
                "password": "$2b$10$l/hYkIAs1ySeq7Wrc.2E4uNOQl5BLxCBOm4Wt1siZPAENsvqt3sOu",
                "email": "superadmin@sicepat.com",
                "firstName": "Super",
                "lastName": "Admin",
                "roleId": "60ebed49127d0700116f5969",
                "createdAt": "2021-07-12T07:20:42.041Z",
                "updatedAt": "2021-07-12T07:20:42.041Z",
                "__v": 0,
                "role": {
                    "_id": "60ebed49127d0700116f5969",
                    "roleName": "Super Admin",
                    "roleAccess": {
                        "_id": "61401adf8fcbf658dcf78bcc",
                        "user": {
                            "_id": "61401adf8fcbf658dcf78bbe",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "role": {
                            "_id": "61401adf8fcbf658dcf78bbf",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "news": {
                            "_id": "61401adf8fcbf658dcf78bc0",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "newsCategory": {
                            "_id": "61401adf8fcbf658dcf78bc1",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "bannerVideo": {
                            "_id": "61401adf8fcbf658dcf78bc2",
                            "update": true
                        },
                        "webinar": {
                            "_id": "61401adf8fcbf658dcf78bc3",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "packageInfo": {
                            "_id": "61401adf8fcbf658dcf78bc4",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "medicalFacility": {
                            "_id": "61401adf8fcbf658dcf78bc5",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "medicalFacilityRegion": {
                            "_id": "61401adf8fcbf658dcf78bc6",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "oxygen": {
                            "_id": "61401adf8fcbf658dcf78bc7",
                            "read": true,
                            "update": true
                        },
                        "mediaEmbed": {
                            "_id": "61401adf8fcbf658dcf78bc8",
                            "create": true,
                            "read": true,
                            "update": true,
                            "delete": true
                        },
                        "scholarshipApplicant": {
                            "_id": "61401adf8fcbf658dcf78bc9",
                            "update": true,
                            "read": true
                        },
                        "scholarshipPayment": {
                            "_id": "61401adf8fcbf658dcf78bca",
                            "update": true,
                            "read": true,
                            "create": true,
                            "delete": true
                        },
                        "scholarshipTier": {
                            "_id": "61401adf8fcbf658dcf78bcb",
                            "create": true,
                            "read": true,
                            "delete": true,
                            "update": true
                        }
                    },
                    "__v": 0
                }
            },
            "roleAccess": [
                "user.create",
                "user.read",
                "user.update",
                "user.delete",
                "role.create",
                "role.read",
                "role.update",
                "role.delete",
                "news.create",
                "news.read",
                "news.update",
                "news.delete",
                "newsCategory.create",
                "newsCategory.read",
                "newsCategory.update",
                "newsCategory.delete",
                "bannerVideo.update",
                "webinar.create",
                "webinar.read",
                "webinar.update",
                "webinar.delete",
                "packageInfo.create",
                "packageInfo.read",
                "packageInfo.update",
                "packageInfo.delete",
                "medicalFacility.create",
                "medicalFacility.read",
                "medicalFacility.update",
                "medicalFacility.delete",
                "medicalFacilityRegion.create",
                "medicalFacilityRegion.read",
                "medicalFacilityRegion.update",
                "medicalFacilityRegion.delete",
                "oxygen.read",
                "oxygen.update",
                "mediaEmbed.create",
                "mediaEmbed.read",
                "mediaEmbed.update",
                "mediaEmbed.delete",
                "scholarshipApplicant.update",
                "scholarshipApplicant.read",
                "scholarshipPayment.update",
                "scholarshipPayment.read",
                "scholarshipPayment.create",
                "scholarshipPayment.delete",
                "scholarshipTier.create",
                "scholarshipTier.read",
                "scholarshipTier.delete",
                "scholarshipTier.update"
            ],
            "access_token": "8739e4cb12a27efbded38ec74379ba16b869976c"
        }
    ],
    "required": [
        "accessToken",
        "accessTokenExpiresAt",
        "refreshToken",
        "refreshTokenExpiresAt",
        "client",
        "user",
        "roleAccess",
        "access_token"
    ],
    "properties": {
        "accessToken": {
            "$id": "#/properties/accessToken",
            "type": "string",
            "title": "The accessToken schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "8739e4cb12a27efbded38ec74379ba16b869976c"
            ]
        },
        "accessTokenExpiresAt": {
            "$id": "#/properties/accessTokenExpiresAt",
            "type": "string",
            "title": "The accessTokenExpiresAt schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "2021-12-14T07:10:36.509Z"
            ]
        },
        "refreshToken": {
            "$id": "#/properties/refreshToken",
            "type": "string",
            "title": "The refreshToken schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "c343dfe0405dda3365ce830cced13596016cc087"
            ]
        },
        "refreshTokenExpiresAt": {
            "$id": "#/properties/refreshTokenExpiresAt",
            "type": "string",
            "title": "The refreshTokenExpiresAt schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "2021-12-14T07:10:36.509Z"
            ]
        },
        "client": {
            "$id": "#/properties/client",
            "type": "object",
            "title": "The client schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "id": "60ec2cc85f2aa900111e207f",
                    "clientId": "indonesiapastibisa-internal",
                    "clientSecret": "S!C3P4T",
                    "grants": [
                        "password",
                        "refresh_token",
                        "authorization_code"
                    ]
                }
            ],
            "required": [
                "id",
                "clientId",
                "clientSecret",
                "grants"
            ],
            "properties": {
                "id": {
                    "$id": "#/properties/client/properties/id",
                    "type": "string",
                    "title": "The id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "60ec2cc85f2aa900111e207f"
                    ]
                },
                "clientId": {
                    "$id": "#/properties/client/properties/clientId",
                    "type": "string",
                    "title": "The clientId schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "indonesiapastibisa-internal"
                    ]
                },
                "clientSecret": {
                    "$id": "#/properties/client/properties/clientSecret",
                    "type": "string",
                    "title": "The clientSecret schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "S!C3P4T"
                    ]
                },
                "grants": {
                    "$id": "#/properties/client/properties/grants",
                    "type": "array",
                    "title": "The grants schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": [],
                    "examples": [
                        [
                            "password",
                            "refresh_token"
                        ]
                    ],
                    "additionalItems": true,
                    "items": {
                        "$id": "#/properties/client/properties/grants/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/client/properties/grants/items/anyOf/0",
                                "type": "string",
                                "title": "The first anyOf schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "password",
                                    "refresh_token"
                                ]
                            }
                        ]
                    }
                }
            },
            "additionalProperties": true
        },
        "user": {
            "$id": "#/properties/user",
            "type": "object",
            "title": "The user schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "_id": "60ebed4a127d0700116f596b",
                    "username": "superadmin",
                    "password": "$2b$10$l/hYkIAs1ySeq7Wrc.2E4uNOQl5BLxCBOm4Wt1siZPAENsvqt3sOu",
                    "email": "superadmin@sicepat.com",
                    "firstName": "Super",
                    "lastName": "Admin",
                    "roleId": "60ebed49127d0700116f5969",
                    "createdAt": "2021-07-12T07:20:42.041Z",
                    "updatedAt": "2021-07-12T07:20:42.041Z",
                    "__v": 0,
                    "role": {
                        "_id": "60ebed49127d0700116f5969",
                        "roleName": "Super Admin",
                        "roleAccess": {
                            "_id": "61401adf8fcbf658dcf78bcc",
                            "user": {
                                "_id": "61401adf8fcbf658dcf78bbe",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "role": {
                                "_id": "61401adf8fcbf658dcf78bbf",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "news": {
                                "_id": "61401adf8fcbf658dcf78bc0",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "newsCategory": {
                                "_id": "61401adf8fcbf658dcf78bc1",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "bannerVideo": {
                                "_id": "61401adf8fcbf658dcf78bc2",
                                "update": true
                            },
                            "webinar": {
                                "_id": "61401adf8fcbf658dcf78bc3",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "packageInfo": {
                                "_id": "61401adf8fcbf658dcf78bc4",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "medicalFacility": {
                                "_id": "61401adf8fcbf658dcf78bc5",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "medicalFacilityRegion": {
                                "_id": "61401adf8fcbf658dcf78bc6",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "oxygen": {
                                "_id": "61401adf8fcbf658dcf78bc7",
                                "read": true,
                                "update": true
                            },
                            "mediaEmbed": {
                                "_id": "61401adf8fcbf658dcf78bc8",
                                "create": true,
                                "read": true,
                                "update": true,
                                "delete": true
                            },
                            "scholarshipApplicant": {
                                "_id": "61401adf8fcbf658dcf78bc9",
                                "update": true,
                                "read": true
                            },
                            "scholarshipPayment": {
                                "_id": "61401adf8fcbf658dcf78bca",
                                "update": true,
                                "read": true,
                                "create": true,
                                "delete": true
                            },
                            "scholarshipTier": {
                                "_id": "61401adf8fcbf658dcf78bcb",
                                "create": true,
                                "read": true,
                                "delete": true,
                                "update": true
                            }
                        },
                        "__v": 0
                    }
                }
            ],
            "required": [
                "_id",
                "username",
                "password",
                "email",
                "firstName",
                "lastName",
                "roleId",
                "createdAt",
                "updatedAt",
                "__v",
                "role"
            ],
            "properties": {
                "_id": {
                    "$id": "#/properties/user/properties/_id",
                    "type": "string",
                    "title": "The _id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "60ebed4a127d0700116f596b"
                    ]
                },
                "username": {
                    "$id": "#/properties/user/properties/username",
                    "type": "string",
                    "title": "The username schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "superadmin"
                    ]
                },
                "password": {
                    "$id": "#/properties/user/properties/password",
                    "type": "string",
                    "title": "The password schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "$2b$10$l/hYkIAs1ySeq7Wrc.2E4uNOQl5BLxCBOm4Wt1siZPAENsvqt3sOu"
                    ]
                },
                "email": {
                    "$id": "#/properties/user/properties/email",
                    "type": "string",
                    "title": "The email schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "superadmin@sicepat.com"
                    ]
                },
                "firstName": {
                    "$id": "#/properties/user/properties/firstName",
                    "type": "string",
                    "title": "The firstName schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "Super"
                    ]
                },
                "lastName": {
                    "$id": "#/properties/user/properties/lastName",
                    "type": "string",
                    "title": "The lastName schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "Admin"
                    ]
                },
                "roleId": {
                    "$id": "#/properties/user/properties/roleId",
                    "type": "string",
                    "title": "The roleId schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "60ebed49127d0700116f5969"
                    ]
                },
                "createdAt": {
                    "$id": "#/properties/user/properties/createdAt",
                    "type": "string",
                    "title": "The createdAt schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "2021-07-12T07:20:42.041Z"
                    ]
                },
                "updatedAt": {
                    "$id": "#/properties/user/properties/updatedAt",
                    "type": "string",
                    "title": "The updatedAt schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "2021-07-12T07:20:42.041Z"
                    ]
                },
                "__v": {
                    "$id": "#/properties/user/properties/__v",
                    "type": "integer",
                    "title": "The __v schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        0
                    ]
                },
                "role": {
                    "$id": "#/properties/user/properties/role",
                    "type": "object",
                    "title": "The role schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": {},
                    "examples": [
                        {
                            "_id": "60ebed49127d0700116f5969",
                            "roleName": "Super Admin",
                            "roleAccess": {
                                "_id": "61401adf8fcbf658dcf78bcc",
                                "user": {
                                    "_id": "61401adf8fcbf658dcf78bbe",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "role": {
                                    "_id": "61401adf8fcbf658dcf78bbf",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "news": {
                                    "_id": "61401adf8fcbf658dcf78bc0",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "newsCategory": {
                                    "_id": "61401adf8fcbf658dcf78bc1",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "bannerVideo": {
                                    "_id": "61401adf8fcbf658dcf78bc2",
                                    "update": true
                                },
                                "webinar": {
                                    "_id": "61401adf8fcbf658dcf78bc3",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "packageInfo": {
                                    "_id": "61401adf8fcbf658dcf78bc4",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "medicalFacility": {
                                    "_id": "61401adf8fcbf658dcf78bc5",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "medicalFacilityRegion": {
                                    "_id": "61401adf8fcbf658dcf78bc6",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "oxygen": {
                                    "_id": "61401adf8fcbf658dcf78bc7",
                                    "read": true,
                                    "update": true
                                },
                                "mediaEmbed": {
                                    "_id": "61401adf8fcbf658dcf78bc8",
                                    "create": true,
                                    "read": true,
                                    "update": true,
                                    "delete": true
                                },
                                "scholarshipApplicant": {
                                    "_id": "61401adf8fcbf658dcf78bc9",
                                    "update": true,
                                    "read": true
                                },
                                "scholarshipPayment": {
                                    "_id": "61401adf8fcbf658dcf78bca",
                                    "update": true,
                                    "read": true,
                                    "create": true,
                                    "delete": true
                                },
                                "scholarshipTier": {
                                    "_id": "61401adf8fcbf658dcf78bcb",
                                    "create": true,
                                    "read": true,
                                    "delete": true,
                                    "update": true
                                }
                            },
                            "__v": 0
                        }
                    ],
                    "required": [
                        "_id",
                        "roleName",
                        "roleAccess",
                        "__v"
                    ],
                    "properties": {
                        "_id": {
                            "$id": "#/properties/user/properties/role/properties/_id",
                            "type": "string",
                            "title": "The _id schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": "",
                            "examples": [
                                "60ebed49127d0700116f5969"
                            ]
                        },
                        "roleName": {
                            "$id": "#/properties/user/properties/role/properties/roleName",
                            "type": "string",
                            "title": "The roleName schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": "",
                            "examples": [
                                "Super Admin"
                            ]
                        },
                        "roleAccess": {
                            "$id": "#/properties/user/properties/role/properties/roleAccess",
                            "type": "object",
                            "title": "The roleAccess schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": {},
                            "examples": [
                                {
                                    "_id": "61401adf8fcbf658dcf78bcc",
                                    "user": {
                                        "_id": "61401adf8fcbf658dcf78bbe",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "role": {
                                        "_id": "61401adf8fcbf658dcf78bbf",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "news": {
                                        "_id": "61401adf8fcbf658dcf78bc0",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "newsCategory": {
                                        "_id": "61401adf8fcbf658dcf78bc1",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "bannerVideo": {
                                        "_id": "61401adf8fcbf658dcf78bc2",
                                        "update": true
                                    },
                                    "webinar": {
                                        "_id": "61401adf8fcbf658dcf78bc3",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "packageInfo": {
                                        "_id": "61401adf8fcbf658dcf78bc4",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "medicalFacility": {
                                        "_id": "61401adf8fcbf658dcf78bc5",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "medicalFacilityRegion": {
                                        "_id": "61401adf8fcbf658dcf78bc6",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "oxygen": {
                                        "_id": "61401adf8fcbf658dcf78bc7",
                                        "read": true,
                                        "update": true
                                    },
                                    "mediaEmbed": {
                                        "_id": "61401adf8fcbf658dcf78bc8",
                                        "create": true,
                                        "read": true,
                                        "update": true,
                                        "delete": true
                                    },
                                    "scholarshipApplicant": {
                                        "_id": "61401adf8fcbf658dcf78bc9",
                                        "update": true,
                                        "read": true
                                    },
                                    "scholarshipPayment": {
                                        "_id": "61401adf8fcbf658dcf78bca",
                                        "update": true,
                                        "read": true,
                                        "create": true,
                                        "delete": true
                                    },
                                    "scholarshipTier": {
                                        "_id": "61401adf8fcbf658dcf78bcb",
                                        "create": true,
                                        "read": true,
                                        "delete": true,
                                        "update": true
                                    }
                                }
                            ],
                            "required": [
                                "_id",
                                "user",
                                "role",
                                "news",
                                "newsCategory",
                                "bannerVideo",
                                "webinar",
                                "packageInfo",
                                "medicalFacility",
                                "medicalFacilityRegion",
                                "oxygen",
                                "mediaEmbed",
                                "scholarshipApplicant",
                                "scholarshipPayment",
                                "scholarshipTier"
                            ],
                            "properties": {
                                "_id": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/_id",
                                    "type": "string",
                                    "title": "The _id schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": "",
                                    "examples": [
                                        "61401adf8fcbf658dcf78bcc"
                                    ]
                                },
                                "user": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/user",
                                    "type": "object",
                                    "title": "The user schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bbe",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/user/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bbe"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/user/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/user/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/user/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/user/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "role": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/role",
                                    "type": "object",
                                    "title": "The role schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bbf",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/role/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bbf"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/role/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/role/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/role/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/role/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "news": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/news",
                                    "type": "object",
                                    "title": "The news schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc0",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/news/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc0"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/news/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/news/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/news/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/news/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "newsCategory": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/newsCategory",
                                    "type": "object",
                                    "title": "The newsCategory schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc1",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/newsCategory/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc1"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/newsCategory/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/newsCategory/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/newsCategory/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/newsCategory/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "bannerVideo": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/bannerVideo",
                                    "type": "object",
                                    "title": "The bannerVideo schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc2",
                                            "update": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "update"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/bannerVideo/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc2"
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/bannerVideo/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "webinar": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/webinar",
                                    "type": "object",
                                    "title": "The webinar schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc3",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/webinar/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc3"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/webinar/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/webinar/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/webinar/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/webinar/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "packageInfo": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/packageInfo",
                                    "type": "object",
                                    "title": "The packageInfo schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc4",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/packageInfo/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc4"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/packageInfo/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/packageInfo/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/packageInfo/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/packageInfo/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "medicalFacility": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacility",
                                    "type": "object",
                                    "title": "The medicalFacility schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc5",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacility/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc5"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacility/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacility/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacility/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacility/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "medicalFacilityRegion": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacilityRegion",
                                    "type": "object",
                                    "title": "The medicalFacilityRegion schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc6",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacilityRegion/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc6"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacilityRegion/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacilityRegion/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacilityRegion/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/medicalFacilityRegion/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "oxygen": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/oxygen",
                                    "type": "object",
                                    "title": "The oxygen schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc7",
                                            "read": true,
                                            "update": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "read",
                                        "update"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/oxygen/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc7"
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/oxygen/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/oxygen/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "mediaEmbed": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/mediaEmbed",
                                    "type": "object",
                                    "title": "The mediaEmbed schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc8",
                                            "create": true,
                                            "read": true,
                                            "update": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "update",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/mediaEmbed/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc8"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/mediaEmbed/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/mediaEmbed/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/mediaEmbed/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/mediaEmbed/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "scholarshipApplicant": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipApplicant",
                                    "type": "object",
                                    "title": "The scholarshipApplicant schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bc9",
                                            "update": true,
                                            "read": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "update",
                                        "read"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipApplicant/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bc9"
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipApplicant/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipApplicant/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "scholarshipPayment": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipPayment",
                                    "type": "object",
                                    "title": "The scholarshipPayment schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bca",
                                            "update": true,
                                            "read": true,
                                            "create": true,
                                            "delete": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "update",
                                        "read",
                                        "create",
                                        "delete"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipPayment/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bca"
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipPayment/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipPayment/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipPayment/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipPayment/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                },
                                "scholarshipTier": {
                                    "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipTier",
                                    "type": "object",
                                    "title": "The scholarshipTier schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "61401adf8fcbf658dcf78bcb",
                                            "create": true,
                                            "read": true,
                                            "delete": true,
                                            "update": true
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "create",
                                        "read",
                                        "delete",
                                        "update"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipTier/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "61401adf8fcbf658dcf78bcb"
                                            ]
                                        },
                                        "create": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipTier/properties/create",
                                            "type": "boolean",
                                            "title": "The create schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "read": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipTier/properties/read",
                                            "type": "boolean",
                                            "title": "The read schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "delete": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipTier/properties/delete",
                                            "type": "boolean",
                                            "title": "The delete schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        },
                                        "update": {
                                            "$id": "#/properties/user/properties/role/properties/roleAccess/properties/scholarshipTier/properties/update",
                                            "type": "boolean",
                                            "title": "The update schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": false,
                                            "examples": [
                                                true
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                }
                            },
                            "additionalProperties": true
                        },
                        "__v": {
                            "$id": "#/properties/user/properties/role/properties/__v",
                            "type": "integer",
                            "title": "The __v schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0,
                            "examples": [
                                0
                            ]
                        }
                    },
                    "additionalProperties": true
                }
            },
            "additionalProperties": true
        },
        "roleAccess": {
            "$id": "#/properties/roleAccess",
            "type": "array",
            "title": "The roleAccess schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    "user.create",
                    "user.read"
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/roleAccess/items",
                "anyOf": [
                    {
                        "$id": "#/properties/roleAccess/items/anyOf/0",
                        "type": "string",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "user.create",
                            "user.read"
                        ]
                    }
                ]
            }
        },
        "access_token": {
            "$id": "#/properties/access_token",
            "type": "string",
            "title": "The access_token schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "8739e4cb12a27efbded38ec74379ba16b869976c"
            ]
        }
    },
    "additionalProperties": true
};

module.exports = {
    schemaLogin
}