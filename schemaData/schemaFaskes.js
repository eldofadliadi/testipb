const schemaNewCategoryFaskes = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "name": "jest test"
        }
    ],
    "required": [
        "name"
    ],
    "properties": {
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "jest test"
            ]
        }
    },
    "additionalProperties": true
};

const schemaListRegionFaskes = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "data": [
                {
                    "id": "611f24c6c0886300125e6627",
                    "name": "Bali"
                },
                {
                    "id": "6119d7eb3b0a8e0011bcfd05",
                    "name": "Jawa Barat"
                },
                {
                    "id": "6119d7f23b0a8e0011bcfd07",
                    "name": "Jawa Timur"
                },
                {
                    "id": "611f8033c0886300125e6655",
                    "name": "Maluku"
                },
                {
                    "id": "6128a0bf35e6ce0012f29dd0",
                    "name": "Medan"
                },
                {
                    "id": "61318d1cfd5ffe0011e8238b",
                    "name": "Surabaya"
                },
                {
                    "id": "61e66dcd243ee300129ea98f",
                    "name": "jest test"
                }
            ],
            "take": 30,
            "skip": 0,
            "total": 7
        }
    ],
    "required": [
        "data",
        "take",
        "skip",
        "total"
    ],
    "properties": {
        "data": {
            "$id": "#/properties/data",
            "type": "array",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    {
                        "id": "611f24c6c0886300125e6627",
                        "name": "Bali"
                    },
                    {
                        "id": "6119d7eb3b0a8e0011bcfd05",
                        "name": "Jawa Barat"
                    }
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/data/items",
                "anyOf": [
                    {
                        "$id": "#/properties/data/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "examples": [
                            {
                                "id": "611f24c6c0886300125e6627",
                                "name": "Bali"
                            }
                        ],
                        "required": [
                            "id",
                            "name"
                        ],
                        "properties": {
                            "id": {
                                "$id": "#/properties/data/items/anyOf/0/properties/id",
                                "type": "string",
                                "title": "The id schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "611f24c6c0886300125e6627"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/data/items/anyOf/0/properties/name",
                                "type": "string",
                                "title": "The name schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "Bali"
                                ]
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        },
        "take": {
            "$id": "#/properties/take",
            "type": "integer",
            "title": "The take schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                30
            ]
        },
        "skip": {
            "$id": "#/properties/skip",
            "type": "integer",
            "title": "The skip schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "total": {
            "$id": "#/properties/total",
            "type": "integer",
            "title": "The total schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                7
            ]
        }
    },
    "additionalProperties": true
};

const schemaDetailRegion = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "id": "61e66dcd243ee300129ea98f",
            "name": "jest test"
        }
    ],
    "required": [
        "id",
        "name"
    ],
    "properties": {
        "id": {
            "$id": "#/properties/id",
            "type": "string",
            "title": "The id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "61e66dcd243ee300129ea98f"
            ]
        },
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "jest test"
            ]
        }
    },
    "additionalProperties": true
};

const schemaNewLokasiFaskes = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "regionId": "611f24c6c0886300125e6627",
            "name": "test lokasi",
            "city": "Bali",
            "address": "Alamat di bali",
            "latitude": 0,
            "longitude": 0
        }
    ],
    "required": [
        "regionId",
        "name",
        "city",
        "address",
        "latitude",
        "longitude"
    ],
    "properties": {
        "regionId": {
            "$id": "#/properties/regionId",
            "type": "string",
            "title": "The regionId schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "611f24c6c0886300125e6627"
            ]
        },
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test lokasi"
            ]
        },
        "city": {
            "$id": "#/properties/city",
            "type": "string",
            "title": "The city schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Bali"
            ]
        },
        "address": {
            "$id": "#/properties/address",
            "type": "string",
            "title": "The address schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Alamat di bali"
            ]
        },
        "latitude": {
            "$id": "#/properties/latitude",
            "type": "integer",
            "title": "The latitude schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "longitude": {
            "$id": "#/properties/longitude",
            "type": "integer",
            "title": "The longitude schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        }
    },
    "additionalProperties": true
};

const schemaListLokasiFaskes = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "data": [
                {
                    "id": "61e7825abc31080013d22da8",
                    "name": "test lokasi",
                    "city": "Bali",
                    "address": "Alamat di bali",
                    "latitude": 0,
                    "longitude": 0,
                    "region": {
                        "id": "611f24c6c0886300125e6627",
                        "name": "Bali"
                    }
                }
            ],
            "take": 1,
            "skip": 0,
            "total": 14
        }
    ],
    "required": [
        "data",
        "take",
        "skip",
        "total"
    ],
    "properties": {
        "data": {
            "$id": "#/properties/data",
            "type": "array",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    {
                        "id": "61e7825abc31080013d22da8",
                        "name": "test lokasi",
                        "city": "Bali",
                        "address": "Alamat di bali",
                        "latitude": 0,
                        "longitude": 0,
                        "region": {
                            "id": "611f24c6c0886300125e6627",
                            "name": "Bali"
                        }
                    }
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/data/items",
                "anyOf": [
                    {
                        "$id": "#/properties/data/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "examples": [
                            {
                                "id": "61e7825abc31080013d22da8",
                                "name": "test lokasi",
                                "city": "Bali",
                                "address": "Alamat di bali",
                                "latitude": 0,
                                "longitude": 0,
                                "region": {
                                    "id": "611f24c6c0886300125e6627",
                                    "name": "Bali"
                                }
                            }
                        ],
                        "required": [
                            "id",
                            "name",
                            "city",
                            "address",
                            "latitude",
                            "longitude",
                            "region"
                        ],
                        "properties": {
                            "id": {
                                "$id": "#/properties/data/items/anyOf/0/properties/id",
                                "type": "string",
                                "title": "The id schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "61e7825abc31080013d22da8"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/data/items/anyOf/0/properties/name",
                                "type": "string",
                                "title": "The name schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "test lokasi"
                                ]
                            },
                            "city": {
                                "$id": "#/properties/data/items/anyOf/0/properties/city",
                                "type": "string",
                                "title": "The city schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "Bali"
                                ]
                            },
                            "address": {
                                "$id": "#/properties/data/items/anyOf/0/properties/address",
                                "type": "string",
                                "title": "The address schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "Alamat di bali"
                                ]
                            },
                            "latitude": {
                                "$id": "#/properties/data/items/anyOf/0/properties/latitude",
                                "type": "integer",
                                "title": "The latitude schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0,
                                "examples": [
                                    0
                                ]
                            },
                            "longitude": {
                                "$id": "#/properties/data/items/anyOf/0/properties/longitude",
                                "type": "integer",
                                "title": "The longitude schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0,
                                "examples": [
                                    0
                                ]
                            },
                            "region": {
                                "$id": "#/properties/data/items/anyOf/0/properties/region",
                                "type": "object",
                                "title": "The region schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": {},
                                "examples": [
                                    {
                                        "id": "611f24c6c0886300125e6627",
                                        "name": "Bali"
                                    }
                                ],
                                "required": [
                                    "id",
                                    "name"
                                ],
                                "properties": {
                                    "id": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/region/properties/id",
                                        "type": "string",
                                        "title": "The id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "611f24c6c0886300125e6627"
                                        ]
                                    },
                                    "name": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/region/properties/name",
                                        "type": "string",
                                        "title": "The name schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "Bali"
                                        ]
                                    }
                                },
                                "additionalProperties": true
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        },
        "take": {
            "$id": "#/properties/take",
            "type": "integer",
            "title": "The take schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                1
            ]
        },
        "skip": {
            "$id": "#/properties/skip",
            "type": "integer",
            "title": "The skip schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "total": {
            "$id": "#/properties/total",
            "type": "integer",
            "title": "The total schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                14
            ]
        }
    },
    "additionalProperties": true
};

const schemaDetailLokasiFaskes = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "id": "61e7825abc31080013d22da8",
            "name": "test lokasi",
            "city": "Bali",
            "address": "Alamat di bali",
            "latitude": 0,
            "longitude": 0,
            "region": {
                "id": "611f24c6c0886300125e6627",
                "name": "Bali"
            }
        }
    ],
    "required": [
        "id",
        "name",
        "city",
        "address",
        // "latitude",
        // "longitude",
        "region"
    ],
    "properties": {
        "id": {
            "$id": "#/properties/id",
            "type": "string",
            "title": "The id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "61e7825abc31080013d22da8"
            ]
        },
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test lokasi"
            ]
        },
        "city": {
            "$id": "#/properties/city",
            "type": "string",
            "title": "The city schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Bali"
            ]
        },
        "address": {
            "$id": "#/properties/address",
            "type": "string",
            "title": "The address schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Alamat di bali"
            ]
        },
        // "latitude": {
        //     "$id": "#/properties/latitude",
        //     "type": "integer",
        //     "title": "The latitude schema",
        //     "description": "An explanation about the purpose of this instance.",
        //     "default": 0,
        //     "examples": [
        //         0
        //     ]
        // },
        // "longitude": {
        //     "$id": "#/properties/longitude",
        //     "type": "integer",
        //     "title": "The longitude schema",
        //     "description": "An explanation about the purpose of this instance.",
        //     "default": 0,
        //     "examples": [
        //         0
        //     ]
        // },
        "region": {
            "$id": "#/properties/region",
            "type": "object",
            "title": "The region schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "id": "611f24c6c0886300125e6627",
                    "name": "Bali"
                }
            ],
            "required": [
                "id",
                "name"
            ],
            "properties": {
                "id": {
                    "$id": "#/properties/region/properties/id",
                    "type": "string",
                    "title": "The id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "611f24c6c0886300125e6627"
                    ]
                },
                "name": {
                    "$id": "#/properties/region/properties/name",
                    "type": "string",
                    "title": "The name schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "Bali"
                    ]
                }
            },
            "additionalProperties": true
        }
    },
    "additionalProperties": true
};

module.exports = { schemaNewCategoryFaskes,schemaListRegionFaskes,schemaDetailRegion,schemaNewLokasiFaskes,schemaListLokasiFaskes,schemaDetailLokasiFaskes }