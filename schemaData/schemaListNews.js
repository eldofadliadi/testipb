const schemaListNews = {
    "type": "object",
    "title": "The root schema",
    "default": {},
    "examples": [
        {
            "data": [
                {
                    "_id": "61289e0935e6ce0012f29dc8",
                    "newsCategoryIds": [
                        "60ec4dd48fb97000123b3b2b"
                    ],
                    "title": "Cara Kerja Masker N95 Untuk Mencegah Covid-19",
                    "caption": "Masker N95 merupakan salah satu jenis respirator yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko Covid-19.",
                    "description": "<p>Masker N95 merupakan salah satu&nbsp;<a href=\"https://indonesiapastibisa.org/activity/post/6115ecd44314c60011f50d5c-ciri-ciri-masker-yang-tepat-untuk-digunakan\">jenis respirator&nbsp;</a>yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko&nbsp;Covid-19.</p>\n\n<p>Selain itu, karena persediaan masker N95 terbatas,&nbsp;<em>Centers for Disease Control and Prevention</em>&nbsp;mengatakan bahwa masker ini harus disediakan untuk tenaga kesehatan. Mereka harus dilatih dan lulus tes kecocokan sebelum menggunakannya. Seperti masker bedah, masker ini dimaksudkan untuk sekali pakai. Namun, para peneliti kini sedang menguji cara untuk mendesinfeksi dan menggunakannya kembali.</p>\n\n<h2><strong>Cara Kerja Masker N95</strong></h2>\n\n<p>Respirator dan masker bedah N95 adalah contoh alat pelindung diri (APD) yang digunakan untuk melindungi pemakainya dari partikel di udara dan dari cairan yang mencemari wajah. Namun, penting untuk diketahui bahwa cara optimal untuk mencegah penularan melalui udara adalah dengan menggunakan kombinasi intervensi dari seluruh protokol kesehatan, bukan hanya APD saja.</p>\n\n<p>Masker N95 adalah perangkat pelindung&nbsp;pernapasan&nbsp;yang dirancang untuk mencapai kesesuaian wajah yang sangat baik dan penyaringan partikel udara yang sangat efisien. Perhatikan bahwa bagian tepi dari masker ini dirancang untuk membentuk segel di sekitar hidung dan mulut. Masker N95 bedah biasanya digunakan dalam pengaturan perawatan kesehatan dan merupakan bagian dari N95 Filtering Facepiece Respirators (FFR), sering disebut sebagai N95.</p>\n\n<p>Berikut ini dasar-dasar cara memakai masker:</p>\n\n<ul>\n\t<li>Bersihkan tangan&nbsp;sebelum memakai masker, juga sebelum dan sesudah melepasnya, dan setelah kamu menyentuhnya setiap saat.</li>\n\t<li>Pastikan ia menutupi hidung, mulut, dan dagu.</li>\n\t<li>Saat kamu melepas masker, simpan dalam kantong plastik bersih, dan setiap hari cuci jika ia adalah masker kain, atau buang masker medis di tempat sampah.</li>\n\t<li>Jangan gunakan masker yang memiliki katup.</li>\n</ul>\n\n<h2><strong>Hal yang Harus Diperhatikan</strong></h2>\n\n<p>Ada beberapa hal yang perlu diperhatikan mengenai penggunaan masker ini. Yang pertama, masker ini paling diprioritaskan untuk digunakan oleh tenaga kesehatan. Jadi, ada baiknya kamu menggunakan masker lain seperti KN95 atau masker bedah dan masker kain yang dipakai berlapis.</p>\n\n<p>Selain itu, ada beberapa hal lain yang perlu diperhatikan, antara lain:&nbsp;</p>\n\n<ul>\n\t<li>Orang dengan penyakit pernapasan kronis, jantung, atau kondisi medis lain yang membuat sulit bernapas harus berkonsultasi dengan dokter sebelum menggunakan respirator N95 karena respirator N95 dapat mempersulit pemakainya untuk bernapas.</li>\n\t<li>Beberapa model memiliki katup pernapasan yang dapat membuat pernapasan lebih mudah dan membantu mengurangi penumpukan panas. Namun, masker N95 dengan katup<a href=\"https://indonesiapastibisa.org/activity/post/6108eca758ac790012a8872c-cara-pasang-regulator-oksigen-saat-isoman-di-rumah\">&nbsp;pernafasan</a>&nbsp;tidak boleh digunakan ketika kondisi steril diperlukan.</li>\n\t<li>Semua N95 diberi label sebagai perangkat sekali pakai, sekali pakai. Jika ia rusak atau kotor, atau jika pernapasan menjadi sulit, kamu harus melepas respirator, membuangnya dengan benar, dan menggantinya dengan yang baru. Untuk membuang masker ini dengan aman, masukkan ke dalam kantong plastik dan buang ke tempat sampah. Cuci tangan setelah memegang respirator bekas.</li>\n\t<li>Masker N95 tidak dirancang untuk&nbsp;anak-anak&nbsp;atau orang dengan rambut wajah. Ini karena kesesuaian yang tepat tidak dapat dicapai pada anak-anak dan orang-orang dengan rambut wajah, sebab masker ini mungkin tidak mampu memberikan perlindungan penuh pada mereka.</li>\n</ul>\n\n<p><em>Artikel ini telah terbit di Halodoc.com<br />\n<a href=\"https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19\">https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19</a><br />\n5 Agustus 2021<br />\nDitinjau oleh dr. Rizal Fadli</em></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>eqwe</td>\n\t\t\t<td>123</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;</p>\n",
                    "route": "cara-kerja-masker-n95-untuk-mencegah-covid-19",
                    "isPinned": true,
                    "createdAt": "2021-08-27T08:10:49.299Z",
                    "updatedAt": "2021-11-11T02:48:27.902Z",
                    "__v": 0,
                    "mainImageId": "61289e3735e6ce0012f29dca",
                    "mainImage": {
                        "_id": "61289e3735e6ce0012f29dca",
                        "fileName": "jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg",
                        "fileMime": "image/jpeg",
                        "createdAt": "Fri Aug 27 2021 08:11:35 GMT+0000 (Coordinated Universal Time)",
                        "updatedAt": "2021-08-27T08:11:35.848Z",
                        "__v": 0,
                        "fileUrl": "https://sicepat-web-version2-dev.s3.amazonaws.com/attachments/2021/8/27/61289e3735e6ce0012f29dca/jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
                    },
                    "newsCategories": [
                        {
                            "_id": "60ec4dd48fb97000123b3b2b",
                            "name": "Tips dan Trik",
                            "route": "tips-dan-trik",
                            "createdAt": "2021-07-13T14:52:47.305Z",
                            "updatedAt": "2021-10-28T11:14:46.292Z",
                            "__v": 0,
                            "isDeleted": true,
                            "position": 2
                        }
                    ]
                }
            ],
            "take": 1,
            "skip": 0,
            "total": 44
        }
    ],
    "required": [
        "data",
        "take",
        "skip",
        "total"
    ],
    "properties": {
        "data": {
            "$id": "#/properties/data",
            "type": "array",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    {
                        "_id": "61289e0935e6ce0012f29dc8",
                        "newsCategoryIds": [
                            "60ec4dd48fb97000123b3b2b"
                        ],
                        "title": "Cara Kerja Masker N95 Untuk Mencegah Covid-19",
                        "caption": "Masker N95 merupakan salah satu jenis respirator yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko Covid-19.",
                        "description": "<p>Masker N95 merupakan salah satu&nbsp;<a href=\"https://indonesiapastibisa.org/activity/post/6115ecd44314c60011f50d5c-ciri-ciri-masker-yang-tepat-untuk-digunakan\">jenis respirator&nbsp;</a>yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko&nbsp;Covid-19.</p>\n\n<p>Selain itu, karena persediaan masker N95 terbatas,&nbsp;<em>Centers for Disease Control and Prevention</em>&nbsp;mengatakan bahwa masker ini harus disediakan untuk tenaga kesehatan. Mereka harus dilatih dan lulus tes kecocokan sebelum menggunakannya. Seperti masker bedah, masker ini dimaksudkan untuk sekali pakai. Namun, para peneliti kini sedang menguji cara untuk mendesinfeksi dan menggunakannya kembali.</p>\n\n<h2><strong>Cara Kerja Masker N95</strong></h2>\n\n<p>Respirator dan masker bedah N95 adalah contoh alat pelindung diri (APD) yang digunakan untuk melindungi pemakainya dari partikel di udara dan dari cairan yang mencemari wajah. Namun, penting untuk diketahui bahwa cara optimal untuk mencegah penularan melalui udara adalah dengan menggunakan kombinasi intervensi dari seluruh protokol kesehatan, bukan hanya APD saja.</p>\n\n<p>Masker N95 adalah perangkat pelindung&nbsp;pernapasan&nbsp;yang dirancang untuk mencapai kesesuaian wajah yang sangat baik dan penyaringan partikel udara yang sangat efisien. Perhatikan bahwa bagian tepi dari masker ini dirancang untuk membentuk segel di sekitar hidung dan mulut. Masker N95 bedah biasanya digunakan dalam pengaturan perawatan kesehatan dan merupakan bagian dari N95 Filtering Facepiece Respirators (FFR), sering disebut sebagai N95.</p>\n\n<p>Berikut ini dasar-dasar cara memakai masker:</p>\n\n<ul>\n\t<li>Bersihkan tangan&nbsp;sebelum memakai masker, juga sebelum dan sesudah melepasnya, dan setelah kamu menyentuhnya setiap saat.</li>\n\t<li>Pastikan ia menutupi hidung, mulut, dan dagu.</li>\n\t<li>Saat kamu melepas masker, simpan dalam kantong plastik bersih, dan setiap hari cuci jika ia adalah masker kain, atau buang masker medis di tempat sampah.</li>\n\t<li>Jangan gunakan masker yang memiliki katup.</li>\n</ul>\n\n<h2><strong>Hal yang Harus Diperhatikan</strong></h2>\n\n<p>Ada beberapa hal yang perlu diperhatikan mengenai penggunaan masker ini. Yang pertama, masker ini paling diprioritaskan untuk digunakan oleh tenaga kesehatan. Jadi, ada baiknya kamu menggunakan masker lain seperti KN95 atau masker bedah dan masker kain yang dipakai berlapis.</p>\n\n<p>Selain itu, ada beberapa hal lain yang perlu diperhatikan, antara lain:&nbsp;</p>\n\n<ul>\n\t<li>Orang dengan penyakit pernapasan kronis, jantung, atau kondisi medis lain yang membuat sulit bernapas harus berkonsultasi dengan dokter sebelum menggunakan respirator N95 karena respirator N95 dapat mempersulit pemakainya untuk bernapas.</li>\n\t<li>Beberapa model memiliki katup pernapasan yang dapat membuat pernapasan lebih mudah dan membantu mengurangi penumpukan panas. Namun, masker N95 dengan katup<a href=\"https://indonesiapastibisa.org/activity/post/6108eca758ac790012a8872c-cara-pasang-regulator-oksigen-saat-isoman-di-rumah\">&nbsp;pernafasan</a>&nbsp;tidak boleh digunakan ketika kondisi steril diperlukan.</li>\n\t<li>Semua N95 diberi label sebagai perangkat sekali pakai, sekali pakai. Jika ia rusak atau kotor, atau jika pernapasan menjadi sulit, kamu harus melepas respirator, membuangnya dengan benar, dan menggantinya dengan yang baru. Untuk membuang masker ini dengan aman, masukkan ke dalam kantong plastik dan buang ke tempat sampah. Cuci tangan setelah memegang respirator bekas.</li>\n\t<li>Masker N95 tidak dirancang untuk&nbsp;anak-anak&nbsp;atau orang dengan rambut wajah. Ini karena kesesuaian yang tepat tidak dapat dicapai pada anak-anak dan orang-orang dengan rambut wajah, sebab masker ini mungkin tidak mampu memberikan perlindungan penuh pada mereka.</li>\n</ul>\n\n<p><em>Artikel ini telah terbit di Halodoc.com<br />\n<a href=\"https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19\">https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19</a><br />\n5 Agustus 2021<br />\nDitinjau oleh dr. Rizal Fadli</em></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>eqwe</td>\n\t\t\t<td>123</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;</p>\n",
                        "route": "cara-kerja-masker-n95-untuk-mencegah-covid-19",
                        "isPinned": true,
                        "createdAt": "2021-08-27T08:10:49.299Z",
                        "updatedAt": "2021-11-11T02:48:27.902Z",
                        "__v": 0,
                        "mainImageId": "61289e3735e6ce0012f29dca",
                        "mainImage": {
                            "_id": "61289e3735e6ce0012f29dca",
                            "fileName": "jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg",
                            "fileMime": "image/jpeg",
                            "createdAt": "Fri Aug 27 2021 08:11:35 GMT+0000 (Coordinated Universal Time)",
                            "updatedAt": "2021-08-27T08:11:35.848Z",
                            "__v": 0,
                            "fileUrl": "https://sicepat-web-version2-dev.s3.amazonaws.com/attachments/2021/8/27/61289e3735e6ce0012f29dca/jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
                        },
                        "newsCategories": [
                            {
                                "_id": "60ec4dd48fb97000123b3b2b",
                                "name": "Tips dan Trik",
                                "route": "tips-dan-trik",
                                "createdAt": "2021-07-13T14:52:47.305Z",
                                "updatedAt": "2021-10-28T11:14:46.292Z",
                                "__v": 0,
                                "isDeleted": true,
                                "position": 2
                            }
                        ]
                    }
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/data/items",
                "anyOf": [
                    {
                        "$id": "#/properties/data/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "examples": [
                            {
                                "_id": "61289e0935e6ce0012f29dc8",
                                "newsCategoryIds": [
                                    "60ec4dd48fb97000123b3b2b"
                                ],
                                "title": "Cara Kerja Masker N95 Untuk Mencegah Covid-19",
                                "caption": "Masker N95 merupakan salah satu jenis respirator yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko Covid-19.",
                                "description": "<p>Masker N95 merupakan salah satu&nbsp;<a href=\"https://indonesiapastibisa.org/activity/post/6115ecd44314c60011f50d5c-ciri-ciri-masker-yang-tepat-untuk-digunakan\">jenis respirator&nbsp;</a>yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko&nbsp;Covid-19.</p>\n\n<p>Selain itu, karena persediaan masker N95 terbatas,&nbsp;<em>Centers for Disease Control and Prevention</em>&nbsp;mengatakan bahwa masker ini harus disediakan untuk tenaga kesehatan. Mereka harus dilatih dan lulus tes kecocokan sebelum menggunakannya. Seperti masker bedah, masker ini dimaksudkan untuk sekali pakai. Namun, para peneliti kini sedang menguji cara untuk mendesinfeksi dan menggunakannya kembali.</p>\n\n<h2><strong>Cara Kerja Masker N95</strong></h2>\n\n<p>Respirator dan masker bedah N95 adalah contoh alat pelindung diri (APD) yang digunakan untuk melindungi pemakainya dari partikel di udara dan dari cairan yang mencemari wajah. Namun, penting untuk diketahui bahwa cara optimal untuk mencegah penularan melalui udara adalah dengan menggunakan kombinasi intervensi dari seluruh protokol kesehatan, bukan hanya APD saja.</p>\n\n<p>Masker N95 adalah perangkat pelindung&nbsp;pernapasan&nbsp;yang dirancang untuk mencapai kesesuaian wajah yang sangat baik dan penyaringan partikel udara yang sangat efisien. Perhatikan bahwa bagian tepi dari masker ini dirancang untuk membentuk segel di sekitar hidung dan mulut. Masker N95 bedah biasanya digunakan dalam pengaturan perawatan kesehatan dan merupakan bagian dari N95 Filtering Facepiece Respirators (FFR), sering disebut sebagai N95.</p>\n\n<p>Berikut ini dasar-dasar cara memakai masker:</p>\n\n<ul>\n\t<li>Bersihkan tangan&nbsp;sebelum memakai masker, juga sebelum dan sesudah melepasnya, dan setelah kamu menyentuhnya setiap saat.</li>\n\t<li>Pastikan ia menutupi hidung, mulut, dan dagu.</li>\n\t<li>Saat kamu melepas masker, simpan dalam kantong plastik bersih, dan setiap hari cuci jika ia adalah masker kain, atau buang masker medis di tempat sampah.</li>\n\t<li>Jangan gunakan masker yang memiliki katup.</li>\n</ul>\n\n<h2><strong>Hal yang Harus Diperhatikan</strong></h2>\n\n<p>Ada beberapa hal yang perlu diperhatikan mengenai penggunaan masker ini. Yang pertama, masker ini paling diprioritaskan untuk digunakan oleh tenaga kesehatan. Jadi, ada baiknya kamu menggunakan masker lain seperti KN95 atau masker bedah dan masker kain yang dipakai berlapis.</p>\n\n<p>Selain itu, ada beberapa hal lain yang perlu diperhatikan, antara lain:&nbsp;</p>\n\n<ul>\n\t<li>Orang dengan penyakit pernapasan kronis, jantung, atau kondisi medis lain yang membuat sulit bernapas harus berkonsultasi dengan dokter sebelum menggunakan respirator N95 karena respirator N95 dapat mempersulit pemakainya untuk bernapas.</li>\n\t<li>Beberapa model memiliki katup pernapasan yang dapat membuat pernapasan lebih mudah dan membantu mengurangi penumpukan panas. Namun, masker N95 dengan katup<a href=\"https://indonesiapastibisa.org/activity/post/6108eca758ac790012a8872c-cara-pasang-regulator-oksigen-saat-isoman-di-rumah\">&nbsp;pernafasan</a>&nbsp;tidak boleh digunakan ketika kondisi steril diperlukan.</li>\n\t<li>Semua N95 diberi label sebagai perangkat sekali pakai, sekali pakai. Jika ia rusak atau kotor, atau jika pernapasan menjadi sulit, kamu harus melepas respirator, membuangnya dengan benar, dan menggantinya dengan yang baru. Untuk membuang masker ini dengan aman, masukkan ke dalam kantong plastik dan buang ke tempat sampah. Cuci tangan setelah memegang respirator bekas.</li>\n\t<li>Masker N95 tidak dirancang untuk&nbsp;anak-anak&nbsp;atau orang dengan rambut wajah. Ini karena kesesuaian yang tepat tidak dapat dicapai pada anak-anak dan orang-orang dengan rambut wajah, sebab masker ini mungkin tidak mampu memberikan perlindungan penuh pada mereka.</li>\n</ul>\n\n<p><em>Artikel ini telah terbit di Halodoc.com<br />\n<a href=\"https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19\">https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19</a><br />\n5 Agustus 2021<br />\nDitinjau oleh dr. Rizal Fadli</em></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>eqwe</td>\n\t\t\t<td>123</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;</p>\n",
                                "route": "cara-kerja-masker-n95-untuk-mencegah-covid-19",
                                "isPinned": true,
                                "createdAt": "2021-08-27T08:10:49.299Z",
                                "updatedAt": "2021-11-11T02:48:27.902Z",
                                "__v": 0,
                                "mainImageId": "61289e3735e6ce0012f29dca",
                                "mainImage": {
                                    "_id": "61289e3735e6ce0012f29dca",
                                    "fileName": "jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg",
                                    "fileMime": "image/jpeg",
                                    "createdAt": "Fri Aug 27 2021 08:11:35 GMT+0000 (Coordinated Universal Time)",
                                    "updatedAt": "2021-08-27T08:11:35.848Z",
                                    "__v": 0,
                                    "fileUrl": "https://sicepat-web-version2-dev.s3.amazonaws.com/attachments/2021/8/27/61289e3735e6ce0012f29dca/jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
                                },
                                "newsCategories": [
                                    {
                                        "_id": "60ec4dd48fb97000123b3b2b",
                                        "name": "Tips dan Trik",
                                        "route": "tips-dan-trik",
                                        "createdAt": "2021-07-13T14:52:47.305Z",
                                        "updatedAt": "2021-10-28T11:14:46.292Z",
                                        "__v": 0,
                                        "isDeleted": true,
                                        "position": 2
                                    }
                                ]
                            }
                        ],
                        "required": [
                            "_id",
                            "newsCategoryIds",
                            "title",
                            "caption",
                            "description",
                            "route",
                            "isPinned",
                            "createdAt",
                            "updatedAt",
                            "__v",
                            "mainImageId",
                            "mainImage",
                            "newsCategories"
                        ],
                        "properties": {
                            "_id": {
                                "$id": "#/properties/data/items/anyOf/0/properties/_id",
                                "type": "string",
                                "title": "The _id schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "61289e0935e6ce0012f29dc8"
                                ]
                            },
                            "newsCategoryIds": {
                                "$id": "#/properties/data/items/anyOf/0/properties/newsCategoryIds",
                                "type": "array",
                                "title": "The newsCategoryIds schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": [],
                                "examples": [
                                    [
                                        "60ec4dd48fb97000123b3b2b"
                                    ]
                                ],
                                "additionalItems": true,
                                "items": {
                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategoryIds/items",
                                    "anyOf": [
                                        {
                                            "$id": "#/properties/data/items/anyOf/0/properties/newsCategoryIds/items/anyOf/0",
                                            "type": "string",
                                            "title": "The first anyOf schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "60ec4dd48fb97000123b3b2b"
                                            ]
                                        }
                                    ]
                                }
                            },
                            "title": {
                                "$id": "#/properties/data/items/anyOf/0/properties/title",
                                "type": "string",
                                "title": "The title schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "Cara Kerja Masker N95 Untuk Mencegah Covid-19"
                                ]
                            },
                            "caption": {
                                "$id": "#/properties/data/items/anyOf/0/properties/caption",
                                "type": "string",
                                "title": "The caption schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "Masker N95 merupakan salah satu jenis respirator yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko Covid-19."
                                ]
                            },
                            "description": {
                                "$id": "#/properties/data/items/anyOf/0/properties/description",
                                "type": "string",
                                "title": "The description schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "<p>Masker N95 merupakan salah satu&nbsp;<a href=\"https://indonesiapastibisa.org/activity/post/6115ecd44314c60011f50d5c-ciri-ciri-masker-yang-tepat-untuk-digunakan\">jenis respirator&nbsp;</a>yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko&nbsp;Covid-19.</p>\n\n<p>Selain itu, karena persediaan masker N95 terbatas,&nbsp;<em>Centers for Disease Control and Prevention</em>&nbsp;mengatakan bahwa masker ini harus disediakan untuk tenaga kesehatan. Mereka harus dilatih dan lulus tes kecocokan sebelum menggunakannya. Seperti masker bedah, masker ini dimaksudkan untuk sekali pakai. Namun, para peneliti kini sedang menguji cara untuk mendesinfeksi dan menggunakannya kembali.</p>\n\n<h2><strong>Cara Kerja Masker N95</strong></h2>\n\n<p>Respirator dan masker bedah N95 adalah contoh alat pelindung diri (APD) yang digunakan untuk melindungi pemakainya dari partikel di udara dan dari cairan yang mencemari wajah. Namun, penting untuk diketahui bahwa cara optimal untuk mencegah penularan melalui udara adalah dengan menggunakan kombinasi intervensi dari seluruh protokol kesehatan, bukan hanya APD saja.</p>\n\n<p>Masker N95 adalah perangkat pelindung&nbsp;pernapasan&nbsp;yang dirancang untuk mencapai kesesuaian wajah yang sangat baik dan penyaringan partikel udara yang sangat efisien. Perhatikan bahwa bagian tepi dari masker ini dirancang untuk membentuk segel di sekitar hidung dan mulut. Masker N95 bedah biasanya digunakan dalam pengaturan perawatan kesehatan dan merupakan bagian dari N95 Filtering Facepiece Respirators (FFR), sering disebut sebagai N95.</p>\n\n<p>Berikut ini dasar-dasar cara memakai masker:</p>\n\n<ul>\n\t<li>Bersihkan tangan&nbsp;sebelum memakai masker, juga sebelum dan sesudah melepasnya, dan setelah kamu menyentuhnya setiap saat.</li>\n\t<li>Pastikan ia menutupi hidung, mulut, dan dagu.</li>\n\t<li>Saat kamu melepas masker, simpan dalam kantong plastik bersih, dan setiap hari cuci jika ia adalah masker kain, atau buang masker medis di tempat sampah.</li>\n\t<li>Jangan gunakan masker yang memiliki katup.</li>\n</ul>\n\n<h2><strong>Hal yang Harus Diperhatikan</strong></h2>\n\n<p>Ada beberapa hal yang perlu diperhatikan mengenai penggunaan masker ini. Yang pertama, masker ini paling diprioritaskan untuk digunakan oleh tenaga kesehatan. Jadi, ada baiknya kamu menggunakan masker lain seperti KN95 atau masker bedah dan masker kain yang dipakai berlapis.</p>\n\n<p>Selain itu, ada beberapa hal lain yang perlu diperhatikan, antara lain:&nbsp;</p>\n\n<ul>\n\t<li>Orang dengan penyakit pernapasan kronis, jantung, atau kondisi medis lain yang membuat sulit bernapas harus berkonsultasi dengan dokter sebelum menggunakan respirator N95 karena respirator N95 dapat mempersulit pemakainya untuk bernapas.</li>\n\t<li>Beberapa model memiliki katup pernapasan yang dapat membuat pernapasan lebih mudah dan membantu mengurangi penumpukan panas. Namun, masker N95 dengan katup<a href=\"https://indonesiapastibisa.org/activity/post/6108eca758ac790012a8872c-cara-pasang-regulator-oksigen-saat-isoman-di-rumah\">&nbsp;pernafasan</a>&nbsp;tidak boleh digunakan ketika kondisi steril diperlukan.</li>\n\t<li>Semua N95 diberi label sebagai perangkat sekali pakai, sekali pakai. Jika ia rusak atau kotor, atau jika pernapasan menjadi sulit, kamu harus melepas respirator, membuangnya dengan benar, dan menggantinya dengan yang baru. Untuk membuang masker ini dengan aman, masukkan ke dalam kantong plastik dan buang ke tempat sampah. Cuci tangan setelah memegang respirator bekas.</li>\n\t<li>Masker N95 tidak dirancang untuk&nbsp;anak-anak&nbsp;atau orang dengan rambut wajah. Ini karena kesesuaian yang tepat tidak dapat dicapai pada anak-anak dan orang-orang dengan rambut wajah, sebab masker ini mungkin tidak mampu memberikan perlindungan penuh pada mereka.</li>\n</ul>\n\n<p><em>Artikel ini telah terbit di Halodoc.com<br />\n<a href=\"https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19\">https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19</a><br />\n5 Agustus 2021<br />\nDitinjau oleh dr. Rizal Fadli</em></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>eqwe</td>\n\t\t\t<td>123</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;</p>\n"
                                ]
                            },
                            "route": {
                                "$id": "#/properties/data/items/anyOf/0/properties/route",
                                "type": "string",
                                "title": "The route schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "cara-kerja-masker-n95-untuk-mencegah-covid-19"
                                ]
                            },
                            "isPinned": {
                                "$id": "#/properties/data/items/anyOf/0/properties/isPinned",
                                "type": "boolean",
                                "title": "The isPinned schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": false,
                                "examples": [
                                    true
                                ]
                            },
                            "createdAt": {
                                "$id": "#/properties/data/items/anyOf/0/properties/createdAt",
                                "type": "string",
                                "title": "The createdAt schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "2021-08-27T08:10:49.299Z"
                                ]
                            },
                            "updatedAt": {
                                "$id": "#/properties/data/items/anyOf/0/properties/updatedAt",
                                "type": "string",
                                "title": "The updatedAt schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "2021-11-11T02:48:27.902Z"
                                ]
                            },
                            "__v": {
                                "$id": "#/properties/data/items/anyOf/0/properties/__v",
                                "type": "integer",
                                "title": "The __v schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0,
                                "examples": [
                                    0
                                ]
                            },
                            "mainImageId": {
                                "$id": "#/properties/data/items/anyOf/0/properties/mainImageId",
                                "type": "string",
                                "title": "The mainImageId schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "61289e3735e6ce0012f29dca"
                                ]
                            },
                            "mainImage": {
                                "$id": "#/properties/data/items/anyOf/0/properties/mainImage",
                                "type": "object",
                                "title": "The mainImage schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": {},
                                "examples": [
                                    {
                                        "_id": "61289e3735e6ce0012f29dca",
                                        "fileName": "jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg",
                                        "fileMime": "image/jpeg",
                                        "createdAt": "Fri Aug 27 2021 08:11:35 GMT+0000 (Coordinated Universal Time)",
                                        "updatedAt": "2021-08-27T08:11:35.848Z",
                                        "__v": 0,
                                        "fileUrl": "https://sicepat-web-version2-dev.s3.amazonaws.com/attachments/2021/8/27/61289e3735e6ce0012f29dca/jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
                                    }
                                ],
                                "required": [
                                    "_id",
                                    "fileName",
                                    "fileMime",
                                    "createdAt",
                                    "updatedAt",
                                    "__v",
                                    "fileUrl"
                                ],
                                "properties": {
                                    "_id": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/_id",
                                        "type": "string",
                                        "title": "The _id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "61289e3735e6ce0012f29dca"
                                        ]
                                    },
                                    "fileName": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/fileName",
                                        "type": "string",
                                        "title": "The fileName schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
                                        ]
                                    },
                                    "fileMime": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/fileMime",
                                        "type": "string",
                                        "title": "The fileMime schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "image/jpeg"
                                        ]
                                    },
                                    "createdAt": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/createdAt",
                                        "type": "string",
                                        "title": "The createdAt schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "Fri Aug 27 2021 08:11:35 GMT+0000 (Coordinated Universal Time)"
                                        ]
                                    },
                                    "updatedAt": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/updatedAt",
                                        "type": "string",
                                        "title": "The updatedAt schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "2021-08-27T08:11:35.848Z"
                                        ]
                                    },
                                    "__v": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/__v",
                                        "type": "integer",
                                        "title": "The __v schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0,
                                        "examples": [
                                            0
                                        ]
                                    },
                                    "fileUrl": {
                                        "$id": "#/properties/data/items/anyOf/0/properties/mainImage/properties/fileUrl",
                                        "type": "string",
                                        "title": "The fileUrl schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "https://sicepat-web-version2-dev.s3.amazonaws.com/attachments/2021/8/27/61289e3735e6ce0012f29dca/jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
                                        ]
                                    }
                                },
                                "additionalProperties": true
                            },
                            "newsCategories": {
                                "$id": "#/properties/data/items/anyOf/0/properties/newsCategories",
                                "type": "array",
                                "title": "The newsCategories schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": [],
                                "examples": [
                                    [
                                        {
                                            "_id": "60ec4dd48fb97000123b3b2b",
                                            "name": "Tips dan Trik",
                                            "route": "tips-dan-trik",
                                            "createdAt": "2021-07-13T14:52:47.305Z",
                                            "updatedAt": "2021-10-28T11:14:46.292Z",
                                            "__v": 0,
                                            "isDeleted": true,
                                            "position": 2
                                        }
                                    ]
                                ],
                                "additionalItems": true,
                                "items": {
                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items",
                                    "anyOf": [
                                        {
                                            "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0",
                                            "type": "object",
                                            "title": "The first anyOf schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": {},
                                            "examples": [
                                                {
                                                    "_id": "60ec4dd48fb97000123b3b2b",
                                                    "name": "Tips dan Trik",
                                                    "route": "tips-dan-trik",
                                                    "createdAt": "2021-07-13T14:52:47.305Z",
                                                    "updatedAt": "2021-10-28T11:14:46.292Z",
                                                    "__v": 0,
                                                    "isDeleted": true,
                                                    "position": 2
                                                }
                                            ],
                                            "required": [
                                                "_id",
                                                "name",
                                                "route",
                                                "createdAt",
                                                "updatedAt",
                                                "__v",
                                                "isDeleted",
                                                "position"
                                            ],
                                            "properties": {
                                                "_id": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/_id",
                                                    "type": "string",
                                                    "title": "The _id schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": "",
                                                    "examples": [
                                                        "60ec4dd48fb97000123b3b2b"
                                                    ]
                                                },
                                                "name": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/name",
                                                    "type": "string",
                                                    "title": "The name schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": "",
                                                    "examples": [
                                                        "Tips dan Trik"
                                                    ]
                                                },
                                                "route": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/route",
                                                    "type": "string",
                                                    "title": "The route schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": "",
                                                    "examples": [
                                                        "tips-dan-trik"
                                                    ]
                                                },
                                                "createdAt": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/createdAt",
                                                    "type": "string",
                                                    "title": "The createdAt schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": "",
                                                    "examples": [
                                                        "2021-07-13T14:52:47.305Z"
                                                    ]
                                                },
                                                "updatedAt": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/updatedAt",
                                                    "type": "string",
                                                    "title": "The updatedAt schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": "",
                                                    "examples": [
                                                        "2021-10-28T11:14:46.292Z"
                                                    ]
                                                },
                                                "__v": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/__v",
                                                    "type": "integer",
                                                    "title": "The __v schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": 0,
                                                    "examples": [
                                                        0
                                                    ]
                                                },
                                                "isDeleted": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/isDeleted",
                                                    "type": "boolean",
                                                    "title": "The isDeleted schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": false,
                                                    "examples": [
                                                        true
                                                    ]
                                                },
                                                "position": {
                                                    "$id": "#/properties/data/items/anyOf/0/properties/newsCategories/items/anyOf/0/properties/position",
                                                    "type": "integer",
                                                    "title": "The position schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": 0,
                                                    "examples": [
                                                        2
                                                    ]
                                                }
                                            },
                                            "additionalProperties": true
                                        }
                                    ]
                                }
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        },
        "take": {
            "$id": "#/properties/take",
            "type": "integer",
            "title": "The take schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                1
            ]
        },
        "skip": {
            "$id": "#/properties/skip",
            "type": "integer",
            "title": "The skip schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "total": {
            "$id": "#/properties/total",
            "type": "integer",
            "title": "The total schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                44
            ]
        }
    },
    "additionalProperties": true
};

const detailNews = {
    "_id": "61289e0935e6ce0012f29dc8",
    "newsCategoryIds": [
      "60ec4dd48fb97000123b3b2b"
    ],
    "title": "Cara Kerja Masker N95 Untuk Mencegah Covid-19",
    "caption": "Masker N95 merupakan salah satu jenis respirator yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko Covid-19.",
    "description": "<p>Masker N95 merupakan salah satu&nbsp;<a href=\"https://indonesiapastibisa.org/activity/post/6115ecd44314c60011f50d5c-ciri-ciri-masker-yang-tepat-untuk-digunakan\">jenis respirator&nbsp;</a>yang menawarkan perlindungan yang lebih baik daripada masker medis biasa. Ini karena masker N95 mampu menyaring partikel besar dan kecil saat pemakainya menghirup napas. Ia memiliki efektivitas pencegahan hingga 95 persen. Meski tidak sempurna, tetapi masker memang ditujukan untuk mengurangi risiko&nbsp;Covid-19.</p>\n\n<p>Selain itu, karena persediaan masker N95 terbatas,&nbsp;<em>Centers for Disease Control and Prevention</em>&nbsp;mengatakan bahwa masker ini harus disediakan untuk tenaga kesehatan. Mereka harus dilatih dan lulus tes kecocokan sebelum menggunakannya. Seperti masker bedah, masker ini dimaksudkan untuk sekali pakai. Namun, para peneliti kini sedang menguji cara untuk mendesinfeksi dan menggunakannya kembali.</p>\n\n<h2><strong>Cara Kerja Masker N95</strong></h2>\n\n<p>Respirator dan masker bedah N95 adalah contoh alat pelindung diri (APD) yang digunakan untuk melindungi pemakainya dari partikel di udara dan dari cairan yang mencemari wajah. Namun, penting untuk diketahui bahwa cara optimal untuk mencegah penularan melalui udara adalah dengan menggunakan kombinasi intervensi dari seluruh protokol kesehatan, bukan hanya APD saja.</p>\n\n<p>Masker N95 adalah perangkat pelindung&nbsp;pernapasan&nbsp;yang dirancang untuk mencapai kesesuaian wajah yang sangat baik dan penyaringan partikel udara yang sangat efisien. Perhatikan bahwa bagian tepi dari masker ini dirancang untuk membentuk segel di sekitar hidung dan mulut. Masker N95 bedah biasanya digunakan dalam pengaturan perawatan kesehatan dan merupakan bagian dari N95 Filtering Facepiece Respirators (FFR), sering disebut sebagai N95.</p>\n\n<p>Berikut ini dasar-dasar cara memakai masker:</p>\n\n<ul>\n\t<li>Bersihkan tangan&nbsp;sebelum memakai masker, juga sebelum dan sesudah melepasnya, dan setelah kamu menyentuhnya setiap saat.</li>\n\t<li>Pastikan ia menutupi hidung, mulut, dan dagu.</li>\n\t<li>Saat kamu melepas masker, simpan dalam kantong plastik bersih, dan setiap hari cuci jika ia adalah masker kain, atau buang masker medis di tempat sampah.</li>\n\t<li>Jangan gunakan masker yang memiliki katup.</li>\n</ul>\n\n<h2><strong>Hal yang Harus Diperhatikan</strong></h2>\n\n<p>Ada beberapa hal yang perlu diperhatikan mengenai penggunaan masker ini. Yang pertama, masker ini paling diprioritaskan untuk digunakan oleh tenaga kesehatan. Jadi, ada baiknya kamu menggunakan masker lain seperti KN95 atau masker bedah dan masker kain yang dipakai berlapis.</p>\n\n<p>Selain itu, ada beberapa hal lain yang perlu diperhatikan, antara lain:&nbsp;</p>\n\n<ul>\n\t<li>Orang dengan penyakit pernapasan kronis, jantung, atau kondisi medis lain yang membuat sulit bernapas harus berkonsultasi dengan dokter sebelum menggunakan respirator N95 karena respirator N95 dapat mempersulit pemakainya untuk bernapas.</li>\n\t<li>Beberapa model memiliki katup pernapasan yang dapat membuat pernapasan lebih mudah dan membantu mengurangi penumpukan panas. Namun, masker N95 dengan katup<a href=\"https://indonesiapastibisa.org/activity/post/6108eca758ac790012a8872c-cara-pasang-regulator-oksigen-saat-isoman-di-rumah\">&nbsp;pernafasan</a>&nbsp;tidak boleh digunakan ketika kondisi steril diperlukan.</li>\n\t<li>Semua N95 diberi label sebagai perangkat sekali pakai, sekali pakai. Jika ia rusak atau kotor, atau jika pernapasan menjadi sulit, kamu harus melepas respirator, membuangnya dengan benar, dan menggantinya dengan yang baru. Untuk membuang masker ini dengan aman, masukkan ke dalam kantong plastik dan buang ke tempat sampah. Cuci tangan setelah memegang respirator bekas.</li>\n\t<li>Masker N95 tidak dirancang untuk&nbsp;anak-anak&nbsp;atau orang dengan rambut wajah. Ini karena kesesuaian yang tepat tidak dapat dicapai pada anak-anak dan orang-orang dengan rambut wajah, sebab masker ini mungkin tidak mampu memberikan perlindungan penuh pada mereka.</li>\n</ul>\n\n<p><em>Artikel ini telah terbit di Halodoc.com<br />\n<a href=\"https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19\">https://www.halodoc.com/artikel/cara-kerja-masker-n95-untuk-mencegah-covid-19</a><br />\n5 Agustus 2021<br />\nDitinjau oleh dr. Rizal Fadli</em></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>eqwe</td>\n\t\t\t<td>123</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;</p>\n",
    "route": "cara-kerja-masker-n95-untuk-mencegah-covid-19",
    "isPinned": true,
    "createdAt": "2021-08-27T08:10:49.299Z",
    "updatedAt": "2021-11-11T02:48:27.902Z",
    "__v": 0,
    "mainImageId": "61289e3735e6ce0012f29dca",
    "newsCategories": [
      {
        "_id": "60ec4dd48fb97000123b3b2b",
        "name": "Tips dan Trik",
        "route": "tips-dan-trik",
        "createdAt": "2021-07-13T14:52:47.305Z",
        "updatedAt": "2021-10-28T11:14:46.292Z",
        "__v": 0,
        "isDeleted": true,
        "position": 2
      }
    ],
    "mainImage": {
      "_id": "61289e3735e6ce0012f29dca",
      "fileName": "jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg",
      "fileMime": "image/jpeg",
      "createdAt": "Fri Aug 27 2021 08:11:35 GMT+0000 (Coordinated Universal Time)",
      "updatedAt": "2021-08-27T08:11:35.848Z",
      "__v": 0,
      "fileUrl": "https://sicepat-web-version2-dev.s3.amazonaws.com/attachments/2021/8/27/61289e3735e6ce0012f29dca/jonathan-j-castellon-HH3nNZVcXik-unsplash.jpg"
    }
};

module.exports = {
    schemaListNews, detailNews
}