const schemaUpdateNews = {
        "type": "object",
        "title": "The root schema",
        "default": {},
        "examples": [
            {
                "name": "string",
                "newsCategoryIds": [
                    "string"
                ],
                "title": "string",
                "caption": "string",
                "description": "string",
                "route": "string",
                "mainImage": "string",
                "useMainImage": true,
                "userId": "string",
                "createAt": "string",
                "updateAt": "string",
                "isPinned": true
            }
        ],
        "required": [
            "name",
            "newsCategoryIds",
            "title",
            "caption",
            "description",
            "route",
            "userId",
            "isPinned"
        ],
        "properties": {
            "name": {
                "$id": "#/properties/name",
                "type": "string",
                "title": "The name schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "newsCategoryIds": {
                "$id": "#/properties/newsCategoryIds",
                "type": "array",
                "title": "The newsCategoryIds schema",
                "description": "An explanation about the purpose of this instance.",
                "default": [],
                "examples": [
                    [
                        "string"
                    ]
                ],
                "additionalItems": true,
                "items": {
                    "$id": "#/properties/newsCategoryIds/items",
                    "anyOf": [
                        {
                            "$id": "#/properties/newsCategoryIds/items/anyOf/0",
                            "type": "string",
                            "title": "The first anyOf schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": "",
                            "examples": [
                                "string"
                            ]
                        }
                    ]
                }
            },
            "title": {
                "$id": "#/properties/title",
                "type": "string",
                "title": "The title schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "caption": {
                "$id": "#/properties/caption",
                "type": "string",
                "title": "The caption schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "description": {
                "$id": "#/properties/description",
                "type": "string",
                "title": "The description schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "route": {
                "$id": "#/properties/route",
                "type": "string",
                "title": "The route schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "mainImage": {
                "$id": "#/properties/mainImage",
                "type": "string",
                "title": "The mainImage schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "useMainImage": {
                "$id": "#/properties/useMainImage",
                "type": "boolean",
                "title": "The useMainImage schema",
                "description": "An explanation about the purpose of this instance.",
                "default": false,
                "examples": [
                    true
                ]
            },
            "userId": {
                "$id": "#/properties/userId",
                "type": "string",
                "title": "The userId schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "createAt": {
                "$id": "#/properties/createAt",
                "type": "string",
                "title": "The createAt schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "updateAt": {
                "$id": "#/properties/updateAt",
                "type": "string",
                "title": "The updateAt schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "string"
                ]
            },
            "isPinned": {
                "$id": "#/properties/isPinned",
                "type": "boolean",
                "title": "The isPinned schema",
                "description": "An explanation about the purpose of this instance.",
                "default": false,
                "examples": [
                    true
                ]
            }
        },
        "additionalProperties": true
};

module.exports = {
    schemaUpdateNews
}