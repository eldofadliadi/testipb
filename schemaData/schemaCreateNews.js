const schemaCreateNews = {
    "type": "object",
    "title": "The root schema",
    "default": {},
    "examples": [
        {
            "newsCategoryIds": [
                "60f7dfa4245c290012ff143a"
            ],
            "_id": "61b722b0b117600012042980",
            "name": "test berita 2",
            "title": "test berita 2",
            "caption": "test berita 2",
            "description": "test berita 2",
            "route": "test-berita-2",
            "isPinned": false,
            "createdAt": "2021-12-13T10:38:40.975Z",
            "updatedAt": "2021-12-13T10:38:40.975Z",
            "__v": 0,
            "id": "61b722b0b117600012042980"
        }
    ],
    "required": [
        "newsCategoryIds",
        "_id",
        "name",
        "title",
        "caption",
        "description",
        "route",
        "isPinned",
        "createdAt",
        "updatedAt",
        "__v",
        "id"
    ],
    "properties": {
        "newsCategoryIds": {
            "$id": "#/properties/newsCategoryIds",
            "type": "array",
            "title": "The newsCategoryIds schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    "60f7dfa4245c290012ff143a"
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/newsCategoryIds/items",
                "anyOf": [
                    {
                        "$id": "#/properties/newsCategoryIds/items/anyOf/0",
                        "type": "string",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "60f7dfa4245c290012ff143a"
                        ]
                    }
                ]
            }
        },
        "_id": {
            "$id": "#/properties/_id",
            "type": "string",
            "title": "The _id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "61b722b0b117600012042980"
            ]
        },
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test berita 2"
            ]
        },
        "title": {
            "$id": "#/properties/title",
            "type": "string",
            "title": "The title schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test berita 2"
            ]
        },
        "caption": {
            "$id": "#/properties/caption",
            "type": "string",
            "title": "The caption schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test berita 2"
            ]
        },
        "description": {
            "$id": "#/properties/description",
            "type": "string",
            "title": "The description schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test berita 2"
            ]
        },
        "route": {
            "$id": "#/properties/route",
            "type": "string",
            "title": "The route schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test-berita-2"
            ]
        },
        "isPinned": {
            "$id": "#/properties/isPinned",
            "type": "boolean",
            "title": "The isPinned schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false,
            "examples": [
                false
            ]
        },
        "createdAt": {
            "$id": "#/properties/createdAt",
            "type": "string",
            "title": "The createdAt schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "2021-12-13T10:38:40.975Z"
            ]
        },
        "updatedAt": {
            "$id": "#/properties/updatedAt",
            "type": "string",
            "title": "The updatedAt schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "2021-12-13T10:38:40.975Z"
            ]
        },
        "__v": {
            "$id": "#/properties/__v",
            "type": "integer",
            "title": "The __v schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "id": {
            "$id": "#/properties/id",
            "type": "string",
            "title": "The id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "61b722b0b117600012042980"
            ]
        }
    },
    "additionalProperties": true
};

module.exports = {
    schemaCreateNews
}