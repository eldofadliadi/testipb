const schemaNewOrder = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "required": [
        "errors",
        "success",
        "data"
    ],
    "properties": {
        "errors": {
            "$id": "#/properties/errors",
            "type": "array",
            "title": "The errors schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/errors/items"
            }
        },
        "success": {
            "$id": "#/properties/success",
            "type": "boolean",
            "title": "The success schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false,
        },
        "data": {
            "$id": "#/properties/data",
            "type": "boolean",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false,
        }
    },
    "additionalProperties": true
};

const schemaListOrder = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "required": [
        "errors",
        "success",
        "data"
    ],
    "properties": {
        "errors": {
            "$id": "#/properties/errors",
            "type": "array",
            "title": "The errors schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/errors/items"
            }
        },
        "success": {
            "$id": "#/properties/success",
            "type": "boolean",
            "title": "The success schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false,
        },
        "data": {
            "$id": "#/properties/data",
            "type": "object",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "required": [
                "list",
                "take",
                "skip",
                "total"
            ],
            "properties": {
                "list": {
                    "$id": "#/properties/data/properties/list",
                    "type": "array",
                    "title": "The list schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": [],
                    "additionalItems": true,
                    "items": {
                        "$id": "#/properties/data/properties/list/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/data/properties/list/items/anyOf/0",
                                "type": "object",
                                "title": "The first anyOf schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": {},
                                "required": [
                                    "_id",
                                    "shipping_label_counter",
                                    "order_histories",
                                    "tenant_id",
                                    "delivery_time",
                                    "is_cod",
                                    "courier",
                                    "service",
                                    "order_number",
                                    "delivery_note",
                                    "delivery_type",
                                    "charge",
                                    "status",
                                    "tracking_info",
                                    "additional_info",
                                    "origin",
                                    "destination",
                                    "items",
                                    "item_summary",
                                    "transaction",
                                    "branch_id",
                                    "dapartment_id",
                                    "user_child_id",
                                    "userId",
                                    "order_create_date",
                                    "createdAt",
                                    "updatedAt",
                                    "__v",
                                    "department",
                                    "branch"
                                ],
                                "properties": {
                                    "_id": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/_id",
                                        "type": "string",
                                        "title": "The _id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "shipping_label_counter": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/shipping_label_counter",
                                        "type": "integer",
                                        "title": "The shipping_label_counter schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "order_histories": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_histories",
                                        "type": "array",
                                        "title": "The order_histories schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": [],
                                        "additionalItems": true,
                                        "items": {
                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_histories/items",
                                            "anyOf": [
                                                {
                                                    "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_histories/items/anyOf/0",
                                                    "type": "object",
                                                    "title": "The first anyOf schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": {},
                                                    "required": [
                                                        "_id",
                                                        "status",
                                                        "status_date"
                                                    ],
                                                    "properties": {
                                                        "_id": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_histories/items/anyOf/0/properties/_id",
                                                            "type": "string",
                                                            "title": "The _id schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": "",
                                                        },
                                                        "status": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_histories/items/anyOf/0/properties/status",
                                                            "type": "integer",
                                                            "title": "The status schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "status_date": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_histories/items/anyOf/0/properties/status_date",
                                                            "type": "string",
                                                            "title": "The status_date schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": ""
                                                        }
                                                    },
                                                    "additionalProperties": true
                                                }
                                            ]
                                        }
                                    },
                                    "tenant_id": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/tenant_id",
                                        "type": "string",
                                        "title": "The tenant_id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    // "delivery_time": {
                                    //     "$id": "#/properties/data/properties/list/items/anyOf/0/properties/delivery_time",
                                    //     "type": "string",
                                    //     "title": "The delivery_time schema",
                                    //     "description": "An explanation about the purpose of this instance.",
                                    //     "default": ""
                                    // },
                                    "is_cod": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/is_cod",
                                        "type": "boolean",
                                        "title": "The is_cod schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": false
                                    },
                                    "courier": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/courier",
                                        "type": "string",
                                        "title": "The courier schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "service": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/service",
                                        "type": "string",
                                        "title": "The service schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                    },
                                    "order_number": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_number",
                                        "type": "string",
                                        "title": "The order_number schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "delivery_note": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/delivery_note",
                                        "type": "string",
                                        "title": "The delivery_note schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "delivery_type": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/delivery_type",
                                        "type": "string",
                                        "title": "The delivery_type schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "charge": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/charge",
                                        "type": "integer",
                                        "title": "The charge schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "status": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/status",
                                        "type": "integer",
                                        "title": "The status schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "tracking_info": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/tracking_info",
                                        "type": "object",
                                        "title": "The tracking_info schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "airwaybill",
                                            "booking_code",
                                            "etd"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/tracking_info/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "airwaybill": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/tracking_info/properties/airwaybill",
                                                "type": "string",
                                                "title": "The airwaybill schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "booking_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/tracking_info/properties/booking_code",
                                                "type": "string",
                                                "title": "The booking_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "etd": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/tracking_info/properties/etd",
                                                "type": "string",
                                                "title": "The etd schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            }
                                        },
                                        "additionalProperties": true
                                    },
                                    "additional_info": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/additional_info",
                                        "type": "object",
                                        "title": "The additional_info schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "courier"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/additional_info/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "courier": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/additional_info/properties/courier",
                                                "type": "object",
                                                "title": "The courier schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": {},
                                                "required": [
                                                    "items",
                                                    "_id"
                                                ],
                                                "properties": {
                                                    "items": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/additional_info/properties/courier/properties/items",
                                                        "type": "array",
                                                        "title": "The items schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": [],
                                                        "additionalItems": true,
                                                        "items": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/additional_info/properties/courier/properties/items/items"
                                                        }
                                                    },
                                                    "_id": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/additional_info/properties/courier/properties/_id",
                                                        "type": "string",
                                                        "title": "The _id schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    }
                                                },
                                                "additionalProperties": true
                                            }
                                        },
                                        "additionalProperties": true
                                    },
                                    "origin": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin",
                                        "type": "object",
                                        "title": "The origin schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "latitude",
                                            "longitude",
                                            "subdistrict_code",
                                            "subdistrict_name",
                                            "city_code",
                                            "city_name",
                                            "province_code",
                                            "province_name",
                                            "note",
                                            "postal_code",
                                            "contact"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "latitude": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/latitude",
                                                "type": "string",
                                                "title": "The latitude schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "longitude": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/longitude",
                                                "type": "string",
                                                "title": "The longitude schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "subdistrict_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/subdistrict_code",
                                                "type": "string",
                                                "title": "The subdistrict_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "subdistrict_name": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/subdistrict_name",
                                                "type": "string",
                                                "title": "The subdistrict_name schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "city_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/city_code",
                                                "type": "string",
                                                "title": "The city_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "city_name": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/city_name",
                                                "type": "string",
                                                "title": "The city_name schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "province_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/province_code",
                                                "type": "string",
                                                "title": "The province_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "province_name": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/province_name",
                                                "type": "string",
                                                "title": "The province_name schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            // "note": {
                                            //     "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/note",
                                            //     "type": "string",
                                            //     "title": "The note schema",
                                            //     "description": "An explanation about the purpose of this instance.",
                                            //     "default": ""
                                            // },
                                            "postal_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/postal_code",
                                                "type": "string",
                                                "title": "The postal_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "contact": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact",
                                                "type": "object",
                                                "title": "The contact schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": {},
                                                "required": [
                                                    "_id",
                                                    "address",
                                                    "email",
                                                    "name",
                                                    "phone",
                                                    "isSendAsCompany"
                                                ],
                                                "properties": {
                                                    "_id": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact/properties/_id",
                                                        "type": "string",
                                                        "title": "The _id schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "address": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact/properties/address",
                                                        "type": "string",
                                                        "title": "The address schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "email": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact/properties/email",
                                                        "type": "string",
                                                        "title": "The email schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "name": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact/properties/name",
                                                        "type": "string",
                                                        "title": "The name schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "phone": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact/properties/phone",
                                                        "type": "string",
                                                        "title": "The phone schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "isSendAsCompany": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/origin/properties/contact/properties/isSendAsCompany",
                                                        "type": "boolean",
                                                        "title": "The isSendAsCompany schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": false
                                                    }
                                                },
                                                "additionalProperties": true
                                            }
                                        },
                                        "additionalProperties": true
                                    },
                                    "destination": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination",
                                        "type": "object",
                                        "title": "The destination schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "latitude",
                                            "longitude",
                                            "subdistrict_code",
                                            "subdistrict_name",
                                            "city_code",
                                            "city_name",
                                            "province_code",
                                            "province_name",
                                            "note",
                                            "postal_code",
                                            "contact"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "latitude": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/latitude",
                                                "type": "string",
                                                "title": "The latitude schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "longitude": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/longitude",
                                                "type": "string",
                                                "title": "The longitude schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "subdistrict_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/subdistrict_code",
                                                "type": "string",
                                                "title": "The subdistrict_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "subdistrict_name": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/subdistrict_name",
                                                "type": "string",
                                                "title": "The subdistrict_name schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "city_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/city_code",
                                                "type": "string",
                                                "title": "The city_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "city_name": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/city_name",
                                                "type": "string",
                                                "title": "The city_name schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "province_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/province_code",
                                                "type": "string",
                                                "title": "The province_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "province_name": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/province_name",
                                                "type": "string",
                                                "title": "The province_name schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            // "note": {
                                            //     "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/note",
                                            //     "type": "string",
                                            //     "title": "The note schema",
                                            //     "description": "An explanation about the purpose of this instance.",
                                            //     "default": ""
                                            // },
                                            "postal_code": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/postal_code",
                                                "type": "string",
                                                "title": "The postal_code schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "contact": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact",
                                                "type": "object",
                                                "title": "The contact schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": {},
                                                "required": [
                                                    "_id",
                                                    "address",
                                                    "email",
                                                    "name",
                                                    "phone",
                                                    "companyName"
                                                ],
                                                "properties": {
                                                    "_id": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact/properties/_id",
                                                        "type": "string",
                                                        "title": "The _id schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "address": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact/properties/address",
                                                        "type": "string",
                                                        "title": "The address schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "email": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact/properties/email",
                                                        "type": "string",
                                                        "title": "The email schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "name": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact/properties/name",
                                                        "type": "string",
                                                        "title": "The name schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "phone": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact/properties/phone",
                                                        "type": "string",
                                                        "title": "The phone schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    },
                                                    "companyName": {
                                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/destination/properties/contact/properties/companyName",
                                                        "type": "string",
                                                        "title": "The companyName schema",
                                                        "description": "An explanation about the purpose of this instance.",
                                                        "default": ""
                                                    }
                                                },
                                                "additionalProperties": true
                                            }
                                        },
                                        "additionalProperties": true
                                    },
                                    "items": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items",
                                        "type": "array",
                                        "title": "The items schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": [],
                                        "additionalItems": true,
                                        "items": {
                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items",
                                            "anyOf": [
                                                {
                                                    "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0",
                                                    "type": "object",
                                                    "title": "The first anyOf schema",
                                                    "description": "An explanation about the purpose of this instance.",
                                                    "default": {},
                                                    "required": [
                                                        "_id",
                                                        "name",
                                                        "description",
                                                        "weight",
                                                        "weight_uom",
                                                        "qty",
                                                        "value",
                                                        "width",
                                                        "height",
                                                        "length",
                                                        "dimension_uom",
                                                        "total_value"
                                                    ],
                                                    "properties": {
                                                        "_id": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/_id",
                                                            "type": "string",
                                                            "title": "The _id schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": ""
                                                        },
                                                        "name": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/name",
                                                            "type": "string",
                                                            "title": "The name schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": ""
                                                        },
                                                        "description": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/description",
                                                            "type": "string",
                                                            "title": "The description schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": ""
                                                        },
                                                        "weight": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/weight",
                                                            "type": "integer",
                                                            "title": "The weight schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "weight_uom": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/weight_uom",
                                                            "type": "string",
                                                            "title": "The weight_uom schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": ""
                                                        },
                                                        "qty": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/qty",
                                                            "type": "integer",
                                                            "title": "The qty schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "value": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/value",
                                                            "type": "integer",
                                                            "title": "The value schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "width": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/width",
                                                            "type": "integer",
                                                            "title": "The width schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "height": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/height",
                                                            "type": "integer",
                                                            "title": "The height schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "length": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/length",
                                                            "type": "integer",
                                                            "title": "The length schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        },
                                                        "dimension_uom": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/dimension_uom",
                                                            "type": "string",
                                                            "title": "The dimension_uom schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": ""
                                                        },
                                                        "total_value": {
                                                            "$id": "#/properties/data/properties/list/items/anyOf/0/properties/items/items/anyOf/0/properties/total_value",
                                                            "type": "integer",
                                                            "title": "The total_value schema",
                                                            "description": "An explanation about the purpose of this instance.",
                                                            "default": 0
                                                        }
                                                    },
                                                    "additionalProperties": true
                                                }
                                            ]
                                        }
                                    },
                                    "item_summary": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/item_summary",
                                        "type": "string",
                                        "title": "The item_summary schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "transaction": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction",
                                        "type": "object",
                                        "title": "The transaction schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "subtotal",
                                            "shipping_charge",
                                            "fee_insurance",
                                            "is_insuranced",
                                            "discount",
                                            "total_value",
                                            "total_cod",
                                            "weight",
                                            "width",
                                            "height",
                                            "length",
                                            "coolie",
                                            "package_category",
                                            "package_content"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "subtotal": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/subtotal",
                                                "type": "integer",
                                                "title": "The subtotal schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "shipping_charge": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/shipping_charge",
                                                "type": "integer",
                                                "title": "The shipping_charge schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "fee_insurance": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/fee_insurance",
                                                "type": "integer",
                                                "title": "The fee_insurance schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "is_insuranced": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/is_insuranced",
                                                "type": "boolean",
                                                "title": "The is_insuranced schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": false
                                            },
                                            "discount": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/discount",
                                                "type": "integer",
                                                "title": "The discount schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "total_value": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/total_value",
                                                "type": "integer",
                                                "title": "The total_value schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "total_cod": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/total_cod",
                                                "type": "integer",
                                                "title": "The total_cod schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "weight": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/weight",
                                                "type": "integer",
                                                "title": "The weight schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "width": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/width",
                                                "type": "integer",
                                                "title": "The width schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "height": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/height",
                                                "type": "integer",
                                                "title": "The height schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "length": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/length",
                                                "type": "integer",
                                                "title": "The length schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "coolie": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/coolie",
                                                "type": "integer",
                                                "title": "The coolie schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            },
                                            "package_category": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/package_category",
                                                "type": "string",
                                                "title": "The package_category schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "package_content": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/transaction/properties/package_content",
                                                "type": "string",
                                                "title": "The package_content schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            }
                                        },
                                        "additionalProperties": true
                                    },
                                    "branch_id": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch_id",
                                        "type": "string",
                                        "title": "The branch_id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "dapartment_id": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/dapartment_id",
                                        "type": "string",
                                        "title": "The dapartment_id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "user_child_id": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/user_child_id",
                                        "type": "string",
                                        "title": "The user_child_id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "userId": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/userId",
                                        "type": "string",
                                        "title": "The userId schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "order_create_date": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/order_create_date",
                                        "type": "string",
                                        "title": "The order_create_date schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "createdAt": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/createdAt",
                                        "type": "string",
                                        "title": "The createdAt schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "updatedAt": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/updatedAt",
                                        "type": "string",
                                        "title": "The updatedAt schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "__v": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/__v",
                                        "type": "integer",
                                        "title": "The __v schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "department": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department",
                                        "type": "object",
                                        "title": "The department schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "companyId",
                                            "departmentCode",
                                            "departmentName",
                                            "createdAt",
                                            "updatedAt",
                                            "__v"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "companyId": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/companyId",
                                                "type": "string",
                                                "title": "The companyId schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "departmentCode": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/departmentCode",
                                                "type": "string",
                                                "title": "The departmentCode schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "departmentName": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/departmentName",
                                                "type": "string",
                                                "title": "The departmentName schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "createdAt": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/createdAt",
                                                "type": "string",
                                                "title": "The createdAt schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "updatedAt": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/updatedAt",
                                                "type": "string",
                                                "title": "The updatedAt schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "__v": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/department/properties/__v",
                                                "type": "integer",
                                                "title": "The __v schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            }
                                        },
                                        "additionalProperties": true
                                    },
                                    "branch": {
                                        "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch",
                                        "type": "object",
                                        "title": "The branch schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": {},
                                        "required": [
                                            "_id",
                                            "companyId",
                                            "branchCode",
                                            "branchName",
                                            "createdAt",
                                            "updatedAt",
                                            "__v"
                                        ],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/_id",
                                                "type": "string",
                                                "title": "The _id schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "companyId": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/companyId",
                                                "type": "string",
                                                "title": "The companyId schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "branchCode": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/branchCode",
                                                "type": "string",
                                                "title": "The branchCode schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "branchName": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/branchName",
                                                "type": "string",
                                                "title": "The branchName schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "createdAt": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/createdAt",
                                                "type": "string",
                                                "title": "The createdAt schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "updatedAt": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/updatedAt",
                                                "type": "string",
                                                "title": "The updatedAt schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": ""
                                            },
                                            "__v": {
                                                "$id": "#/properties/data/properties/list/items/anyOf/0/properties/branch/properties/__v",
                                                "type": "integer",
                                                "title": "The __v schema",
                                                "description": "An explanation about the purpose of this instance.",
                                                "default": 0
                                            }
                                        },
                                        "additionalProperties": true
                                    }
                                },
                                "additionalProperties": true
                            }
                        ]
                    }
                },
                "take": {
                    "$id": "#/properties/data/properties/take",
                    "type": "integer",
                    "title": "The take schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                },
                "skip": {
                    "$id": "#/properties/data/properties/skip",
                    "type": "integer",
                    "title": "The skip schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                },
                "total": {
                    "$id": "#/properties/data/properties/total",
                    "type": "integer",
                    "title": "The total schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                }
            },
            "additionalProperties": true
        }
    },
    "additionalProperties": true
};

const schemaUpdateOrder = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "required": [
        "errors",
        "success",
        "data"
    ],
    "properties": {
        "errors": {
            "$id": "#/properties/errors",
            "type": "array",
            "title": "The errors schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/errors/items"
            }
        },
        "success": {
            "$id": "#/properties/success",
            "type": "boolean",
            "title": "The success schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false,
        },
        "data": {
            "$id": "#/properties/data",
            "type": "object",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "required": [
                "shipping_label_counter",
                "_id",
                "tenant_id",
                "delivery_time",
                "is_cod",
                "courier",
                "service",
                "order_number",
                "delivery_note",
                "delivery_type",
                "charge",
                "status",
                "tracking_info",
                "additional_info",
                "origin",
                "destination",
                "items",
                "item_summary",
                "transaction",
                "branch_id",
                "dapartment_id",
                "user_child_id",
                "userId",
                "order_create_date",
                "createdAt",
                "updatedAt",
                "__v"
            ],
            "properties": {
                "shipping_label_counter": {
                    "$id": "#/properties/data/properties/shipping_label_counter",
                    "type": "integer",
                    "title": "The shipping_label_counter schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                },
                "_id": {
                    "$id": "#/properties/data/properties/_id",
                    "type": "string",
                    "title": "The _id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "tenant_id": {
                    "$id": "#/properties/data/properties/tenant_id",
                    "type": "string",
                    "title": "The tenant_id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "delivery_time": {
                    "$id": "#/properties/data/properties/delivery_time",
                    "type": "string",
                    "title": "The delivery_time schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "is_cod": {
                    "$id": "#/properties/data/properties/is_cod",
                    "type": "boolean",
                    "title": "The is_cod schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false
                },
                "courier": {
                    "$id": "#/properties/data/properties/courier",
                    "type": "string",
                    "title": "The courier schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "service": {
                    "$id": "#/properties/data/properties/service",
                    "type": "string",
                    "title": "The service schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "order_number": {
                    "$id": "#/properties/data/properties/order_number",
                    "type": "string",
                    "title": "The order_number schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "delivery_note": {
                    "$id": "#/properties/data/properties/delivery_note",
                    "type": "string",
                    "title": "The delivery_note schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "delivery_type": {
                    "$id": "#/properties/data/properties/delivery_type",
                    "type": "string",
                    "title": "The delivery_type schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "charge": {
                    "$id": "#/properties/data/properties/charge",
                    "type": "integer",
                    "title": "The charge schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                },
                "status": {
                    "$id": "#/properties/data/properties/status",
                    "type": "integer",
                    "title": "The status schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                },
                "tracking_info": {
                    "$id": "#/properties/data/properties/tracking_info",
                    "type": "object",
                    "title": "The tracking_info schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": {},
                    "required": [
                        "_id",
                        "airwaybill",
                        "booking_code",
                        "etd",
                        "destination_code"
                    ],
                    "properties": {
                        "_id": {
                            "$id": "#/properties/data/properties/tracking_info/properties/_id",
                            "type": "string",
                            "title": "The _id schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "airwaybill": {
                            "$id": "#/properties/data/properties/tracking_info/properties/airwaybill",
                            "type": "string",
                            "title": "The airwaybill schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "booking_code": {
                            "$id": "#/properties/data/properties/tracking_info/properties/booking_code",
                            "type": "string",
                            "title": "The booking_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "etd": {
                            "$id": "#/properties/data/properties/tracking_info/properties/etd",
                            "type": "string",
                            "title": "The etd schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "destination_code": {
                            "$id": "#/properties/data/properties/tracking_info/properties/destination_code",
                            "type": "null",
                            "title": "The destination_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": null
                        }
                    },
                    "additionalProperties": true
                },
                "additional_info": {
                    "$id": "#/properties/data/properties/additional_info",
                    "type": "object",
                    "title": "The additional_info schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": {},
                    "required": [
                        "_id",
                        "courier"
                    ],
                    "properties": {
                        "_id": {
                            "$id": "#/properties/data/properties/additional_info/properties/_id",
                            "type": "string",
                            "title": "The _id schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "courier": {
                            "$id": "#/properties/data/properties/additional_info/properties/courier",
                            "type": "object",
                            "title": "The courier schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": {},
                            "required": [
                                "items",
                                "_id",
                                "pickup_delivery_time",
                                "pickup_merchant_code",
                                "pickup_merchant_name"
                            ],
                            "properties": {
                                "items": {
                                    "$id": "#/properties/data/properties/additional_info/properties/courier/properties/items",
                                    "type": "array",
                                    "title": "The items schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": [],
                                    "additionalItems": true,
                                    "items": {
                                        "$id": "#/properties/data/properties/additional_info/properties/courier/properties/items/items"
                                    }
                                },
                                "_id": {
                                    "$id": "#/properties/data/properties/additional_info/properties/courier/properties/_id",
                                    "type": "string",
                                    "title": "The _id schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "pickup_delivery_time": {
                                    "$id": "#/properties/data/properties/additional_info/properties/courier/properties/pickup_delivery_time",
                                    "type": "string",
                                    "title": "The pickup_delivery_time schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "pickup_merchant_code": {
                                    "$id": "#/properties/data/properties/additional_info/properties/courier/properties/pickup_merchant_code",
                                    "type": "string",
                                    "title": "The pickup_merchant_code schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "pickup_merchant_name": {
                                    "$id": "#/properties/data/properties/additional_info/properties/courier/properties/pickup_merchant_name",
                                    "type": "string",
                                    "title": "The pickup_merchant_name schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                }
                            },
                            "additionalProperties": true
                        }
                    },
                    "additionalProperties": true
                },
                "origin": {
                    "$id": "#/properties/data/properties/origin",
                    "type": "object",
                    "title": "The origin schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": {},
                    "required": [
                        "_id",
                        "latitude",
                        "longitude",
                        "subdistrict_code",
                        "subdistrict_name",
                        "city_code",
                        "city_name",
                        "province_code",
                        "province_name",
                        "note",
                        "postal_code",
                        "contact"
                    ],
                    "properties": {
                        "_id": {
                            "$id": "#/properties/data/properties/origin/properties/_id",
                            "type": "string",
                            "title": "The _id schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "latitude": {
                            "$id": "#/properties/data/properties/origin/properties/latitude",
                            "type": "string",
                            "title": "The latitude schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "longitude": {
                            "$id": "#/properties/data/properties/origin/properties/longitude",
                            "type": "string",
                            "title": "The longitude schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "subdistrict_code": {
                            "$id": "#/properties/data/properties/origin/properties/subdistrict_code",
                            "type": "string",
                            "title": "The subdistrict_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "subdistrict_name": {
                            "$id": "#/properties/data/properties/origin/properties/subdistrict_name",
                            "type": "string",
                            "title": "The subdistrict_name schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "city_code": {
                            "$id": "#/properties/data/properties/origin/properties/city_code",
                            "type": "string",
                            "title": "The city_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "city_name": {
                            "$id": "#/properties/data/properties/origin/properties/city_name",
                            "type": "string",
                            "title": "The city_name schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "province_code": {
                            "$id": "#/properties/data/properties/origin/properties/province_code",
                            "type": "string",
                            "title": "The province_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "province_name": {
                            "$id": "#/properties/data/properties/origin/properties/province_name",
                            "type": "string",
                            "title": "The province_name schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "note": {
                            "$id": "#/properties/data/properties/origin/properties/note",
                            "type": "string",
                            "title": "The note schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "postal_code": {
                            "$id": "#/properties/data/properties/origin/properties/postal_code",
                            "type": "string",
                            "title": "The postal_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "contact": {
                            "$id": "#/properties/data/properties/origin/properties/contact",
                            "type": "object",
                            "title": "The contact schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": {},
                            "required": [
                                "_id",
                                "address",
                                "email",
                                "name",
                                "phone",
                                "isSendAsCompany"
                            ],
                            "properties": {
                                "_id": {
                                    "$id": "#/properties/data/properties/origin/properties/contact/properties/_id",
                                    "type": "string",
                                    "title": "The _id schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "address": {
                                    "$id": "#/properties/data/properties/origin/properties/contact/properties/address",
                                    "type": "string",
                                    "title": "The address schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "email": {
                                    "$id": "#/properties/data/properties/origin/properties/contact/properties/email",
                                    "type": "string",
                                    "title": "The email schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "name": {
                                    "$id": "#/properties/data/properties/origin/properties/contact/properties/name",
                                    "type": "string",
                                    "title": "The name schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "phone": {
                                    "$id": "#/properties/data/properties/origin/properties/contact/properties/phone",
                                    "type": "string",
                                    "title": "The phone schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "isSendAsCompany": {
                                    "$id": "#/properties/data/properties/origin/properties/contact/properties/isSendAsCompany",
                                    "type": "boolean",
                                    "title": "The isSendAsCompany schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": false
                                }
                            },
                            "additionalProperties": true
                        }
                    },
                    "additionalProperties": true
                },
                "destination": {
                    "$id": "#/properties/data/properties/destination",
                    "type": "object",
                    "title": "The destination schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": {},
                    "required": [
                        "_id",
                        "latitude",
                        "longitude",
                        "subdistrict_code",
                        "subdistrict_name",
                        "city_code",
                        "city_name",
                        "province_code",
                        "province_name",
                        "note",
                        "postal_code",
                        "contact"
                    ],
                    "properties": {
                        "_id": {
                            "$id": "#/properties/data/properties/destination/properties/_id",
                            "type": "string",
                            "title": "The _id schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "latitude": {
                            "$id": "#/properties/data/properties/destination/properties/latitude",
                            "type": "string",
                            "title": "The latitude schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "longitude": {
                            "$id": "#/properties/data/properties/destination/properties/longitude",
                            "type": "string",
                            "title": "The longitude schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "subdistrict_code": {
                            "$id": "#/properties/data/properties/destination/properties/subdistrict_code",
                            "type": "string",
                            "title": "The subdistrict_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "subdistrict_name": {
                            "$id": "#/properties/data/properties/destination/properties/subdistrict_name",
                            "type": "string",
                            "title": "The subdistrict_name schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "city_code": {
                            "$id": "#/properties/data/properties/destination/properties/city_code",
                            "type": "string",
                            "title": "The city_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "city_name": {
                            "$id": "#/properties/data/properties/destination/properties/city_name",
                            "type": "string",
                            "title": "The city_name schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "province_code": {
                            "$id": "#/properties/data/properties/destination/properties/province_code",
                            "type": "string",
                            "title": "The province_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "province_name": {
                            "$id": "#/properties/data/properties/destination/properties/province_name",
                            "type": "string",
                            "title": "The province_name schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "note": {
                            "$id": "#/properties/data/properties/destination/properties/note",
                            "type": "string",
                            "title": "The note schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "postal_code": {
                            "$id": "#/properties/data/properties/destination/properties/postal_code",
                            "type": "string",
                            "title": "The postal_code schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "contact": {
                            "$id": "#/properties/data/properties/destination/properties/contact",
                            "type": "object",
                            "title": "The contact schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": {},
                            "required": [
                                "_id",
                                "address",
                                "email",
                                "name",
                                "phone",
                                "companyName"
                            ],
                            "properties": {
                                "_id": {
                                    "$id": "#/properties/data/properties/destination/properties/contact/properties/_id",
                                    "type": "string",
                                    "title": "The _id schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "address": {
                                    "$id": "#/properties/data/properties/destination/properties/contact/properties/address",
                                    "type": "string",
                                    "title": "The address schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "email": {
                                    "$id": "#/properties/data/properties/destination/properties/contact/properties/email",
                                    "type": "string",
                                    "title": "The email schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "name": {
                                    "$id": "#/properties/data/properties/destination/properties/contact/properties/name",
                                    "type": "string",
                                    "title": "The name schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "phone": {
                                    "$id": "#/properties/data/properties/destination/properties/contact/properties/phone",
                                    "type": "string",
                                    "title": "The phone schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                },
                                "companyName": {
                                    "$id": "#/properties/data/properties/destination/properties/contact/properties/companyName",
                                    "type": "string",
                                    "title": "The companyName schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": ""
                                }
                            },
                            "additionalProperties": true
                        }
                    },
                    "additionalProperties": true
                },
                "items": {
                    "$id": "#/properties/data/properties/items",
                    "type": "array",
                    "title": "The items schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": [],
                    "additionalItems": true,
                    "items": {
                        "$id": "#/properties/data/properties/items/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/data/properties/items/items/anyOf/0",
                                "type": "object",
                                "title": "The first anyOf schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": {},
                                "required": [
                                    "_id",
                                    "name",
                                    "description",
                                    "weight",
                                    "weight_uom",
                                    "qty",
                                    "value",
                                    "width",
                                    "height",
                                    "length",
                                    "dimension_uom",
                                    "total_value"
                                ],
                                "properties": {
                                    "_id": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/_id",
                                        "type": "string",
                                        "title": "The _id schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "name": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/name",
                                        "type": "string",
                                        "title": "The name schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "description": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/description",
                                        "type": "string",
                                        "title": "The description schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "weight": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/weight",
                                        "type": "integer",
                                        "title": "The weight schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "weight_uom": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/weight_uom",
                                        "type": "string",
                                        "title": "The weight_uom schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "qty": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/qty",
                                        "type": "integer",
                                        "title": "The qty schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "value": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/value",
                                        "type": "integer",
                                        "title": "The value schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "width": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/width",
                                        "type": "integer",
                                        "title": "The width schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "height": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/height",
                                        "type": "integer",
                                        "title": "The height schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "length": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/length",
                                        "type": "integer",
                                        "title": "The length schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    },
                                    "dimension_uom": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/dimension_uom",
                                        "type": "string",
                                        "title": "The dimension_uom schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": ""
                                    },
                                    "total_value": {
                                        "$id": "#/properties/data/properties/items/items/anyOf/0/properties/total_value",
                                        "type": "integer",
                                        "title": "The total_value schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": 0
                                    }
                                },
                                "additionalProperties": true
                            }
                        ]
                    }
                },
                "item_summary": {
                    "$id": "#/properties/data/properties/item_summary",
                    "type": "string",
                    "title": "The item_summary schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "transaction": {
                    "$id": "#/properties/data/properties/transaction",
                    "type": "object",
                    "title": "The transaction schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": {},
                    "required": [
                        "_id",
                        "subtotal",
                        "shipping_charge",
                        "fee_insurance",
                        "is_insuranced",
                        "discount",
                        "total_value",
                        "total_cod",
                        "weight",
                        "width",
                        "height",
                        "length",
                        "coolie",
                        "package_category",
                        "package_content"
                    ],
                    "properties": {
                        "_id": {
                            "$id": "#/properties/data/properties/transaction/properties/_id",
                            "type": "string",
                            "title": "The _id schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "subtotal": {
                            "$id": "#/properties/data/properties/transaction/properties/subtotal",
                            "type": "integer",
                            "title": "The subtotal schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "shipping_charge": {
                            "$id": "#/properties/data/properties/transaction/properties/shipping_charge",
                            "type": "integer",
                            "title": "The shipping_charge schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "fee_insurance": {
                            "$id": "#/properties/data/properties/transaction/properties/fee_insurance",
                            "type": "integer",
                            "title": "The fee_insurance schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "is_insuranced": {
                            "$id": "#/properties/data/properties/transaction/properties/is_insuranced",
                            "type": "boolean",
                            "title": "The is_insuranced schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": false
                        },
                        "discount": {
                            "$id": "#/properties/data/properties/transaction/properties/discount",
                            "type": "integer",
                            "title": "The discount schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "total_value": {
                            "$id": "#/properties/data/properties/transaction/properties/total_value",
                            "type": "integer",
                            "title": "The total_value schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "total_cod": {
                            "$id": "#/properties/data/properties/transaction/properties/total_cod",
                            "type": "integer",
                            "title": "The total_cod schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "weight": {
                            "$id": "#/properties/data/properties/transaction/properties/weight",
                            "type": "integer",
                            "title": "The weight schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "width": {
                            "$id": "#/properties/data/properties/transaction/properties/width",
                            "type": "integer",
                            "title": "The width schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "height": {
                            "$id": "#/properties/data/properties/transaction/properties/height",
                            "type": "integer",
                            "title": "The height schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "length": {
                            "$id": "#/properties/data/properties/transaction/properties/length",
                            "type": "integer",
                            "title": "The length schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "coolie": {
                            "$id": "#/properties/data/properties/transaction/properties/coolie",
                            "type": "integer",
                            "title": "The coolie schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": 0
                        },
                        "package_category": {
                            "$id": "#/properties/data/properties/transaction/properties/package_category",
                            "type": "string",
                            "title": "The package_category schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        },
                        "package_content": {
                            "$id": "#/properties/data/properties/transaction/properties/package_content",
                            "type": "string",
                            "title": "The package_content schema",
                            "description": "An explanation about the purpose of this instance.",
                            "default": ""
                        }
                    },
                    "additionalProperties": true
                },
                "branch_id": {
                    "$id": "#/properties/data/properties/branch_id",
                    "type": "string",
                    "title": "The branch_id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "dapartment_id": {
                    "$id": "#/properties/data/properties/dapartment_id",
                    "type": "string",
                    "title": "The dapartment_id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "user_child_id": {
                    "$id": "#/properties/data/properties/user_child_id",
                    "type": "string",
                    "title": "The user_child_id schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "userId": {
                    "$id": "#/properties/data/properties/userId",
                    "type": "string",
                    "title": "The userId schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "order_create_date": {
                    "$id": "#/properties/data/properties/order_create_date",
                    "type": "string",
                    "title": "The order_create_date schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "createdAt": {
                    "$id": "#/properties/data/properties/createdAt",
                    "type": "string",
                    "title": "The createdAt schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "updatedAt": {
                    "$id": "#/properties/data/properties/updatedAt",
                    "type": "string",
                    "title": "The updatedAt schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": ""
                },
                "__v": {
                    "$id": "#/properties/data/properties/__v",
                    "type": "integer",
                    "title": "The __v schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0
                }
            },
            "additionalProperties": true
        }
    },
    "additionalProperties": true
};

const schemaFailOrderLocationNotCover = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "required": [
        "status",
        "errorId",
        "message",
        "errors"
    ],
    "properties": {
        "status": {
            "$id": "#/properties/status",
            "type": "integer",
            "title": "The status schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0
        },
        "errorId": {
            "$id": "#/properties/errorId",
            "type": "string",
            "title": "The errorId schema",
            "description": "An explanation about the purpose of this instance.",
            "default": ""
        },
        "message": {
            "$id": "#/properties/message",
            "type": "string",
            "title": "The message schema",
            "description": "An explanation about the purpose of this instance.",
            "default": ""
        },
        "errors": {
            "$id": "#/properties/errors",
            "type": "array",
            "title": "The errors schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/errors/items",
                "anyOf": [
                    {
                        "$id": "#/properties/errors/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "required": [
                            "error_code",
                            "error_message"
                        ],
                        "properties": {
                            "error_code": {
                                "$id": "#/properties/errors/items/anyOf/0/properties/error_code",
                                "type": "integer",
                                "title": "The error_code schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0
                            },
                            "error_message": {
                                "$id": "#/properties/errors/items/anyOf/0/properties/error_message",
                                "type": "string",
                                "title": "The error_message schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": ""
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        }
    },
    "additionalProperties": true
};

const schemaDataDashboard = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "required": [
        "errors",
        "success",
        "data"
    ],
    "properties": {
        "errors": {
            "$id": "#/properties/errors",
            "type": "array",
            "title": "The errors schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/errors/items"
            }
        },
        "success": {
            "$id": "#/properties/success",
            "type": "boolean",
            "title": "The success schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false
        },
        "data": {
            "$id": "#/properties/data",
            "type": "array",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/data/items",
                "anyOf": [
                    {
                        "$id": "#/properties/data/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "required": [
                            "status",
                            "total",
                            "totalCharge"
                        ],
                        "properties": {
                            "status": {
                                "$id": "#/properties/data/items/anyOf/0/properties/status",
                                "type": "string",
                                "title": "The status schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": ""
                            },
                            "total": {
                                "$id": "#/properties/data/items/anyOf/0/properties/total",
                                "type": "integer",
                                "title": "The total schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0
                            },
                            "totalCharge": {
                                "$id": "#/properties/data/items/anyOf/0/properties/totalCharge",
                                "type": "integer",
                                "title": "The totalCharge schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/data/items/anyOf/1",
                        "type": "object",
                        "title": "The second anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "required": [
                            "status",
                            "total"
                        ],
                        "properties": {
                            "status": {
                                "$id": "#/properties/data/items/anyOf/1/properties/status",
                                "type": "string",
                                "title": "The status schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": ""
                            },
                            "total": {
                                "$id": "#/properties/data/items/anyOf/1/properties/total",
                                "type": "integer",
                                "title": "The total schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        }
    },
    "additionalProperties": true
};

schemaDataCity = {
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "required": [
        "errors",
        "success",
        "data"
    ],
    "properties": {
        "errors": {
            "$id": "#/properties/errors",
            "type": "array",
            "title": "The errors schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/errors/items"
            }
        },
        "success": {
            "$id": "#/properties/success",
            "type": "boolean",
            "title": "The success schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false
        },
        "data": {
            "$id": "#/properties/data",
            "type": "array",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/data/items",
                "anyOf": [
                    {
                        "$id": "#/properties/data/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "required": [
                            "_id",
                            "count",
                            "cityName"
                        ],
                        "properties": {
                            "_id": {
                                "$id": "#/properties/data/items/anyOf/0/properties/_id",
                                "type": "string",
                                "title": "The _id schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": ""
                            },
                            "count": {
                                "$id": "#/properties/data/items/anyOf/0/properties/count",
                                "type": "integer",
                                "title": "The count schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0
                            },
                            "cityName": {
                                "$id": "#/properties/data/items/anyOf/0/properties/cityName",
                                "type": "string",
                                "title": "The cityName schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": ""
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        }
    },
    "additionalProperties": true
}

module.exports = {
    schemaNewOrder, schemaListOrder, schemaUpdateOrder, schemaFailOrderLocationNotCover, schemaDataDashboard, schemaDataCity
}