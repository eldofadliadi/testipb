const schemaListCategory = {
    "type": "object",
    "title": "The root schema",
    "default": {},
    "examples": [
        {
            "data": [
                {
                    "_id": "60f7dfa4245c290012ff143a",
                    "name": "Info Covid-19",
                    "route": "info-covid-19",
                    "createdAt": "2021-07-13T14:52:26.527Z",
                    "updatedAt": "2021-07-13T14:52:26.527Z",
                    "__v": 0,
                    "isDeleted": false,
                    "position": 4
                }
            ],
            "take": 1,
            "skip": 0,
            "total": 5
        }
    ],
    "required": [
        "data",
        "take",
        "skip",
        "total"
    ],
    "properties": {
        "data": {
            "$id": "#/properties/data",
            "type": "array",
            "title": "The data schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    {
                        "_id": "60f7dfa4245c290012ff143a",
                        "name": "Info Covid-19",
                        "route": "info-covid-19",
                        "createdAt": "2021-07-13T14:52:26.527Z",
                        "updatedAt": "2021-07-13T14:52:26.527Z",
                        "__v": 0,
                        "isDeleted": false,
                        "position": 4
                    }
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/data/items",
                "anyOf": [
                    {
                        "$id": "#/properties/data/items/anyOf/0",
                        "type": "object",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "examples": [
                            {
                                "_id": "60f7dfa4245c290012ff143a",
                                "name": "Info Covid-19",
                                "route": "info-covid-19",
                                "createdAt": "2021-07-13T14:52:26.527Z",
                                "updatedAt": "2021-07-13T14:52:26.527Z",
                                "__v": 0,
                                "isDeleted": false,
                                "position": 4
                            }
                        ],
                        "required": [
                            "_id",
                            "name",
                            "route",
                            "createdAt",
                            "updatedAt",
                            "__v",
                            "isDeleted",
                            "position"
                        ],
                        "properties": {
                            "_id": {
                                "$id": "#/properties/data/items/anyOf/0/properties/_id",
                                "type": "string",
                                "title": "The _id schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "60f7dfa4245c290012ff143a"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/data/items/anyOf/0/properties/name",
                                "type": "string",
                                "title": "The name schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "Info Covid-19"
                                ]
                            },
                            "route": {
                                "$id": "#/properties/data/items/anyOf/0/properties/route",
                                "type": "string",
                                "title": "The route schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "info-covid-19"
                                ]
                            },
                            "createdAt": {
                                "$id": "#/properties/data/items/anyOf/0/properties/createdAt",
                                "type": "string",
                                "title": "The createdAt schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "2021-07-13T14:52:26.527Z"
                                ]
                            },
                            "updatedAt": {
                                "$id": "#/properties/data/items/anyOf/0/properties/updatedAt",
                                "type": "string",
                                "title": "The updatedAt schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": "",
                                "examples": [
                                    "2021-07-13T14:52:26.527Z"
                                ]
                            },
                            "__v": {
                                "$id": "#/properties/data/items/anyOf/0/properties/__v",
                                "type": "integer",
                                "title": "The __v schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0,
                                "examples": [
                                    0
                                ]
                            },
                            "isDeleted": {
                                "$id": "#/properties/data/items/anyOf/0/properties/isDeleted",
                                "type": "boolean",
                                "title": "The isDeleted schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": false,
                                "examples": [
                                    false
                                ]
                            },
                            "position": {
                                "$id": "#/properties/data/items/anyOf/0/properties/position",
                                "type": "integer",
                                "title": "The position schema",
                                "description": "An explanation about the purpose of this instance.",
                                "default": 0,
                                "examples": [
                                    4
                                ]
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        },
        "take": {
            "$id": "#/properties/take",
            "type": "integer",
            "title": "The take schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                1
            ]
        },
        "skip": {
            "$id": "#/properties/skip",
            "type": "integer",
            "title": "The skip schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "total": {
            "$id": "#/properties/total",
            "type": "integer",
            "title": "The total schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                5
            ]
        }
    },
    "additionalProperties": true
};

const schemaCreateCategory = {
    "type": "object",
    "title": "The root schema",
    "default": {},
    "examples": [
        {
            "name": "test",
            "route": "test"
        }
    ],
    "required": [
        "name",
        "route"
    ],
    "properties": {
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test"
            ]
        },
        "route": {
            "$id": "#/properties/route",
            "type": "string",
            "title": "The route schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "test"
            ]
        }
    },
    "additionalProperties": true
};

const schemaDetailCategory = {
    "type": "object",
    "title": "The root schema",
    "default": {},
    "examples": [
        {
            "isDeleted": false,
            "_id": "60f7dfa4245c290012ff143a",
            "name": "Info Covid-19",
            "route": "info-covid-19",
            "createdAt": "2021-07-13T14:52:26.527Z",
            "updatedAt": "2021-07-13T14:52:26.527Z",
            "__v": 0,
            "position": 4,
            "id": "60f7dfa4245c290012ff143a"
        }
    ],
    "required": [
        "isDeleted",
        "_id",
        "name",
        "route",
        "createdAt",
        "updatedAt",
        "__v",
        "position",
        "id"
    ],
    "properties": {
        "isDeleted": {
            "$id": "#/properties/isDeleted",
            "type": "boolean",
            "title": "The isDeleted schema",
            "description": "An explanation about the purpose of this instance.",
            "default": false,
            "examples": [
                false
            ]
        },
        "_id": {
            "$id": "#/properties/_id",
            "type": "string",
            "title": "The _id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "60f7dfa4245c290012ff143a"
            ]
        },
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The name schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Info Covid-19"
            ]
        },
        "route": {
            "$id": "#/properties/route",
            "type": "string",
            "title": "The route schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "info-covid-19"
            ]
        },
        "createdAt": {
            "$id": "#/properties/createdAt",
            "type": "string",
            "title": "The createdAt schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "2021-07-13T14:52:26.527Z"
            ]
        },
        "updatedAt": {
            "$id": "#/properties/updatedAt",
            "type": "string",
            "title": "The updatedAt schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "2021-07-13T14:52:26.527Z"
            ]
        },
        "__v": {
            "$id": "#/properties/__v",
            "type": "integer",
            "title": "The __v schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        },
        "position": {
            "$id": "#/properties/position",
            "type": "integer",
            "title": "The position schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                4
            ]
        },
        "id": {
            "$id": "#/properties/id",
            "type": "string",
            "title": "The id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "60f7dfa4245c290012ff143a"
            ]
        }
    },
    "additionalProperties": true
};

module.exports = {
    schemaListCategory, schemaCreateCategory, schemaDetailCategory
}