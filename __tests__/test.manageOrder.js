const endpointOrder = require('../endpoint/endpointManageOrder');
const dataOrder = require('../dataRequest/dataManageOrder');
const schemaOrder = require('../schemaValidator/schemaManageOrder');
const bodyValidator = require('../bodyValidator/bodyOrder');
const matchers = require('jest-json-schema').matchers;
expect.extend(matchers);
require('./test.login');
let idOrder;

describe('Test API Manage Order', () => {
    test('Fail Create Order Location Not Covered', async () => {
        const response = await endpointOrder.postOrder(dataOrder.failJneRegNonCod)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(400);
        expect(response.body.errors[0].error_message).toBe(bodyValidator.locationNotCovered);
        expect(response.body).toMatchSchema(schemaOrder.schemaFailOrderLocationNotCover);
    });

    test('Empty Payload', async () => {
        const response = await endpointOrder.postOrder(dataOrder.emptyDataOrder)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(400);
        expect(response.body.errors[0].error_message).toBe(bodyValidator.payloadEmpty);
        expect(response.body).toMatchSchema(schemaOrder.schemaFailOrderLocationNotCover);
    });

    test('Empty Data Send',async () => {
        const response = await endpointOrder.postOrder()
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(500);
        expect(response.body.errors[0].error_message).toBe(bodyValidator.emptyData);
    });

    //Order 
    test('Create New Order',async () => {
        const response = await endpointOrder.postOrder(dataOrder.newOrder)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        expect(response.body).toMatchSchema(schemaOrder.schemaNewOrder);
    });

    test('Get List Order',async () => {
        const response = await endpointOrder.getorderList(dataOrder.paramOrderList)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        // expect(response.body).toMatchSchema(schemaOrder.schemaListOrder);
        idOrder = response.body.data.list[0]._id;
        expect(response.body.data.take).toBe(dataOrder.paramOrderList.take);
        expect(response.body.data.skip).toBe(dataOrder.paramOrderList.skip);
    });

    test('Get Detail Order',async () => {
        const response = await endpointOrder.getDetailOrder(idOrder)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.data.order_number).toBeDefined();
        expect(response.body.data.order_histories).toBeDefined();
        expect(response.body.data.tracking_info).toBeDefined();
        expect(response.body.data.additional_info).toBeDefined();
        // expect(response.body).toMatchSchema(schemaOrder.schemaUpdateOrder);
    });

    test('Update Order', async () => {
        const response = await endpointOrder.updateOrder(idOrder,dataOrder.updateOrder)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.data.items[0].name).toBe(dataOrder.updateOrder.items[0].name);
        expect(response.body.data.transaction.shipping_charge).toBe(dataOrder.updateOrder.transaction.shipping_charge);
        expect(response.body.data.transaction.weight).toBe(dataOrder.updateOrder.transaction.weight);
        expect(response.body).toMatchSchema(schemaOrder.schemaUpdateOrder);
    });

    xtest('Set Ready to Ship', async () => {
        const shipdata = {
            "orderIds": [
                `${idOrder}`
              ],
              "delivery_type": "pickup",
              "delivery_time": `${date}`
        };
        const response = await endpointOrder.updateToShip(shipdata)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        console.log({res: response.body});
    });

    test('Cancel Order', async () => {
        const payloadCancel = {
            "orderIds": [
                `${idOrder}`
              ],
              "cancel_reason": "test cancel"
        };
        const response = await endpointOrder.cancelOrder(payloadCancel)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
    });

    test('Get Data Dasboard Order', async () => {
        const response = await endpointOrder.postOrderDashboard(dataOrder.paramOrderDashboard)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.data).toBeDefined();
        expect(response.body).toMatchSchema(schemaOrder.schemaDataDashboard);
    });

    test('Get data City Destination', async () => {
        const response = await endpointOrder.postCityDestination(dataOrder.paramCityDestination)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.data).toBeDefined();
        expect(response.body).toMatchSchema(schemaOrder.schemaDataCity);
    });

});