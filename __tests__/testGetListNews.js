const endpoint = require('../endpoint/listNews');
// const endpointlogin = require('../endpoint/login');
// const dataLogin = require('../dataRequest/loginRequest');
const dataRequest = require('../dataRequest/getListNewsRequest');
const login = require('../__tests__/testLogin');

// beforeAll(async () => {
//     const response = await endpoint.postLogin(dataLogin.dataLogin);
//     token = response.body.access_token;
// });

test('test get list news', async () => {
    const response = await endpoint.getListNews(dataRequest.paramGetListNews)
    .set('Authorization',`Bearer ${token}`);
    console.log({res : token});
    expect(response.status).toEqual(200);
});