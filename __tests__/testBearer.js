const endpointlogin = require('../endpoint/login');
const schema = require('../schemaData/schemaListNews');
const datarequestlogin = require('../dataRequest/loginRequest');
const bodylogin = require('../bodyValidation/bodyLogin');
const endpointnews = require('../endpoint/listNews');
const datanews = require('../dataRequest/getListNewsRequest');
const schemaCreateNews = require('../schemaData/schemaCreateNews');
const schemaUpdateNew = require('../schemaData/schemaUpdateNews');
const dataPostNews = require('../dataRequest/dataCreateNews');
const endpointCreateNews = require('../endpoint/createNews');
const matcher = require('jest-json-schema').matchers;
expect.extend(matcher);
let token;
let id_news;


beforeAll(async () => {
        const response = await endpointlogin.postLogin(datarequestlogin.dataLogin);
        expect(response.status).toEqual(200);
        token = response.body.accessToken;
});

describe('get list news', () =>{
    test('get all news', async () => {
        const response = await endpointnews.getListNews(datanews.paramGetListNews)
        .set('Authorization', `Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schema.schemaListNews);
    });

    test('get detail news', async () => {
        const response = await endpointnews.detailNews(datanews.idnews)
        .set('Authorization', `Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schema.detailNews);
    });
});

describe('create new data', () => {
    test('create news', async () => {
        const response = await endpointCreateNews.createNews(dataPostNews.postNews)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        expect(response.body).toMatchSchema(schemaCreateNews.schemaCreateNews);
        id_news = response.body.id;
        console.log({res: id_news});
    });

    test('update news', async () => {
        const response = await endpointCreateNews.updateNews(id_news,dataPostNews.updateNews)
        .set('Authorization', `Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schemaCreateNews.schemaCreateNews);
        expect(response.body.name).toBe(dataPostNews.updateNews.name);
        expect(response.body.title).toBe(dataPostNews.updateNews.title);
        expect(response.body.caption).toBe(dataPostNews.updateNews.caption);
        expect(response.body.description).toBe(dataPostNews.updateNews.description);
        expect(response.body.route).toBe(dataPostNews.updateNews.route);
    });

    test('delete news', async () => {
        const response = await endpointCreateNews.deleteNews(id_news)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });

});

