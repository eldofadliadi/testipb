const endPointLogin = require('../endpoint/login');
const dataLogin = require('../dataRequest/loginRequest');
const endPointMedia = require('../endpoint/endPointMediaEmbed');
const dataMedia = require('../dataRequest/dataRequestMediaEmbed');
const matcher = require('jest-json-schema').matchers;
expect.extend(matcher);
let token;
let idMedia;

beforeAll(async()=> {
    const response = await endPointLogin.postLogin(dataLogin.dataLogin);
    expect(response.status).toEqual(200);
    token = response.body.accessToken;
});

describe('Test Menu Media Embed', () => {
    test('get list media embed',async()=>{
        const response = await endPointMedia.getListEmbed(dataMedia.paramGetListEmbed)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });

    test('create media embed', async () => {
        const response = await endPointMedia.createMediaEmbded(dataMedia.createNewEmbed)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        expect(response.body._id).toBeDefined();
        expect(response.body.platform).toBe(dataMedia.createNewEmbed.platform);
        expect(response.body.title).toBe(dataMedia.createNewEmbed.title);
        expect(response.body.embedUrl).toBe(dataMedia.createNewEmbed.embedUrl);
        expect(response.body.isDeleted).toBe(false);
        expect(response.body.createdAt).toBeDefined();
        expect(response.body.updatedAt).toBeDefined();
        expect(response.body.__v).toBe(0);
        idMedia = response.body._id;
    });

    test('get detail media',async () => {
        const response = await endPointMedia.getDetailEmbed(idMedia)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.isDeleted).toBe(false);
        expect(response.body.title).toBe(dataMedia.createNewEmbed.title);
        expect(response.body.embedUrl).toBe(dataMedia.createNewEmbed.embedUrl);
        expect(response.body.platform).toBe(dataMedia.createNewEmbed.platform);
    });

    test('update media embed', async () => {
        const response = await endPointMedia.updateMediaEmbed(idMedia,dataMedia.updateMediaEmbed)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.name).toBe(dataMedia.updateMediaEmbed.name);
    });

    test('delete media embed', async () => {
        const response = await endPointMedia.deleteMediaEmbed(idMedia)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });
});