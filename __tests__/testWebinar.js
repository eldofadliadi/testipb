const endPointWebinar = require('../endpoint/endPointWebinar');
const dataWebinar = require('../dataRequest/dataRequestWebinar');
const endPointLogin = require('../endpoint/login');
const dataLogin = require('../dataRequest/loginRequest');
const matchers = require('jest-json-schema').matchers;
expect.extend(matchers);
require('./testLogin');

// beforeAll(async ()=>{
//     const response = await endPointLogin.postLogin(dataLogin.dataLogin);
//     expect(response.status).toEqual(200);
//     token = response.body.accessToken;
//     console.log(response.body.accessToken);
// });

describe('Test Webinar', () => {
    test('Create Webinar Baru',async () => {
        const response = await endPointWebinar.newWebinar(dataWebinar.newWebinar)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        idWebinar = response.body._id;
        console.log(response.body);
    });

    test('Update Webinar Existing',async () => {
        const response = await endPointWebinar.updateWebinar(idWebinar,dataWebinar.updateWebinar)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        console.log(response.body);
    });
});