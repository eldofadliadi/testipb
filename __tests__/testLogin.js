const endpoint = require('../endpoint/login');
const datarequest = require('../dataRequest/loginRequest');
const body = require('../bodyValidation/bodyLogin');
const schema = require('../schemaData/schemaLogin');
const matcher = require('jest-json-schema').matchers;
expect.extend(matcher);
// let token;
// let tokenexp;
// let refreshtoken;
// let refreshexp;
// let client;

describe('Login User', () => {
    test('Login Valid', async () => {
        const response = await endpoint.postLogin(datarequest.dataLogin);
        expect(response.status).toEqual(200);
        // console.log({res: response.body})
        token = response.body.accessToken;
        // tokenexp = response.body.accessTokenExpiresAt;
        // refreshtoken = response.body.refreshToken;
        // refreshexp = response.body.refreshTokenExpiresAt;
        // client = response.body.client;
        // console.log({res: response.body.access_token});
        // expect(response.body).toEqual(body.s)
        // expect(response.body.access_token).toEqual(token);
        // expect(response.body.accessTokenExpiresAt).toEqual(tokenexp);
        // console.log({res: response.body.accessTokenExpiresAt});
        // expect(response.body.refreshToken).toEqual(refreshtoken);
        // expect(response.body.refreshTokenExpiresAt).toEqual(refreshexp);
        // expect(response.body.client).toEqual(client);
        // console.log({res: client});
        // expect(response.body).toEqual(body.bodyvalidation);
        // expect(response.body.roleAccess).toEqual(body.bodyRoleAccess);
        // expect(response.body.user).toEqual(body.bodyRole);
        expect(response.body).toMatchSchema(schema.schemaLogin);
        // console.log({res: schema.schemaLogin});
        console.log({res: token})
    });
});