const endPointLogin = require ('../endpoint/login');
const dataRequestLogin = require ('../dataRequest/loginRequest');
const endPointPackage = require ('../endpoint/endPointPackage');
const dataPackage = require ('../dataRequest/dataPackage');
const matcher = require ('jest-json-schema').matchers;
expect.extend(matcher);
let token;
let idPackage;

beforeAll(async() => {
    const response = await endPointLogin.postLogin(dataRequestLogin.dataLogin);
    expect(response.status).toEqual(200);
    token = response.body.accessToken;
});

describe('test menu paket info', () => {
    test('get list paket info', async () => {
        const response = await endPointPackage.getListPackage(dataPackage.paramGetPackage)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });

    test('get detail paket info', async () => {
        const response = await endPointPackage.getDetailPackage(dataPackage.paketMakanan._id)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.name).toBe(dataPackage.paketMakanan.name);
        expect(response.body.total).toBe(dataPackage.paketMakanan.total);
    });

    test('create paket info baru', async () => {
        const response = await endPointPackage.createPackage(dataPackage.newPackage)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        expect(response.body._id).toBeDefined();
        expect(response.body.name).toBe(dataPackage.newPackage.name);
        expect(response.body.total).toBe(dataPackage.newPackage.total);
        expect(response.body.createdAt).toBeDefined();
        expect(response.body.isActive).toBeDefined();
        idPackage = response.body._id;
    });

    test('update paket info',async () => {
        const response = await endPointPackage.updatePackage(idPackage,dataPackage.updatePackage)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.name).toBe(dataPackage.updatePackage.name);
        expect(response.body.total).toBe(dataPackage.updatePackage.total);
    });

    test('delete paket info', async () => {
        const response = await endPointPackage.deletePackage(idPackage)
        .set('Authorization', `Bearer ${token}`);
        expect(response.status).toEqual(200);
    });
});