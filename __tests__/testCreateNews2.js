const endPointNews = require('../endpoint/createNews');
const endPointCategory = require('../endpoint/endpointNewsCategory');
const dataNews = require('../dataRequest/dataCreateNews');
const dataCategory = require('../dataRequest/dataCategoryNews');
const endPointLogin = require('../endpoint/login');
const dataLogin = require('../dataRequest/loginRequest');
const matchers = require('jest-json-schema').matchers;
expect.extend(matchers);
let idCategory;
let idNews;
let token;

beforeAll(async() => {
    const response = await endPointLogin.postLogin(dataLogin.dataLogin);
    expect(response.status).toEqual(200);
    token = response.body.accessToken;
    const response2 = await endPointCategory.createCategoryNews(dataCategory.createCategoryNews)
    .set('Authorization',`Bearer ${token}`);
    expect(response2.status).toEqual(201);
    const response3 = await endPointCategory.getListCategoryNews(dataCategory.paramUpdateCategory)
    .set('Authorization',`Bearer ${token}`);
    expect(response3.status).toEqual(200);
    idCategory = response3.body.data[0]._id;
});

describe('Test Create News Baru', () => {
    test('Create News 2',async () => {
        const response = await endPointNews.createNews2(idCategory,dataNews.postNews2)
        .set('Authorization',`Bearer ${token}`);
        console.log({res : response.body});
    });
});

afterAll(async () => {
    const response = await endPointCategory.deleteCategoryNews(idCategory)
    .set('Authorization',`Bearer ${token}`);
    expect(response.status).toEqual(200);
});