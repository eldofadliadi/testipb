const endPointLogin = require('../endpoint/login');
const dataLogin = require('../dataRequest/loginRequest');
const endPointFaskes = require('../endpoint/endPointFaskes');
const dataFaskes = require('../dataRequest/dataFaskes');
const schemaFaskes = require('../schemaData/schemaFaskes');
const matchers = require('jest-json-schema').matchers;
expect.extend(matchers);
let token;
let idRegion;
let idLokasi;

beforeAll(async () => {
    const response = await endPointLogin.postLogin(dataLogin.dataLogin);
    expect(response.status).toEqual(200);
    token = response.body.accessToken;
});

describe('Test Region Fasilitas Kesehatan', () => {
    test('Create Region Baru',async () => {
        const response = await endPointFaskes.createRegionFaskes(dataFaskes.createRegionFaskes)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        expect(response.body).toMatchSchema(schemaFaskes.schemaNewCategoryFaskes);
    });

    test('Get List Fasilitasi Kesehatan',async () => {
        const response = await endPointFaskes.getListRegionFaskses(dataFaskes.paramRegion)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        idRegion = response.body.data[0].id;
        expect(response.body).toMatchSchema(schemaFaskes.schemaListRegionFaskes);
        // console.log({res: response.body.data[0].id});
    });

    test('Update Fasilitas Kesehatan', async () => {
        const response = await endPointFaskes.updateRegionFaskes(idRegion,dataFaskes.updateRegionFaskes)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.name).toBe(dataFaskes.updateRegionFaskes.name);
        expect(response.body).toMatchSchema(schemaFaskes.schemaNewCategoryFaskes);
    });

    test('Get Detail Region Fasilitas Kesehatan', async () => {
        const response = await endPointFaskes.getDetailRegionFaskes(idRegion)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body.name).toBeDefined();
        expect(response.body.id).toBeDefined();
        expect(response.body).toMatchSchema(schemaFaskes.schemaDetailRegion);
    });

    test('Create Lokasi Fasilitas Kesehatan', async () => {
        const payload = {
            "regionId" : idRegion,
            "name": "jest lokasi",
            "city": "jest city",
            "address": "alamat jest lokasi",
            "latitude": 0,
            "longitude": 0
        };
        const response = await endPointFaskes.createLokasiFaskes(payload)
        .set('Authorization',`Bearer ${token}`)
        expect(response.status).toEqual(201);
        // console.log(response.body);
        expect(response.body).toMatchSchema(schemaFaskes.schemaNewLokasiFaskes);
    });

    test('Get List Lokasi Fasilitas Kesehatan', async () => {
        const response = await endPointFaskes.getListLokasiFaskes(dataFaskes.paramLokasi)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        idLokasi = response.body.data[0].id;
        expect(response.body).toMatchSchema(schemaFaskes.schemaListLokasiFaskes);
    });

    test('Update Lokasi Fasilitas Kesehatan',async () => {
        const response = await endPointFaskes.updateLokasiFaskes(idLokasi,dataFaskes.updateLokasiFaskes)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        // console.log({res : response.body});
    });

    test('Get Detail Lokasi Fasilitas Kesehatan', async () => {
        const response = await endPointFaskes.getDetailLokasiFaskes(idLokasi)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schemaFaskes.schemaDetailLokasiFaskes);
        // console.log({body : response.body});
    });

    test('Delete Lokasi Fasilitasi Kesehatan', async () => {
        const response = await endPointFaskes.deleteLokasiFaskes(idLokasi)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });

    test('Delete Region Fasilitas Kesehatan', async () => {
        const response = await endPointFaskes.deleteRegionFaskes(idRegion)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });
});