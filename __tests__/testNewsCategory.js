const endPointLogin = require ('../endpoint/login');
const dataRequestLogin = require ('../dataRequest/loginRequest');
const schemaCategory = require ('../schemaData/schemaCategoryNews');
const endPointCategory = require ('../endpoint/endpointNewsCategory');
const dataRequestCategory = require ('../dataRequest/dataCategoryNews');
const matcher = require('jest-json-schema').matchers;
expect.extend(matcher);
let token;
let idCategoryNews;

beforeAll (async() => {
    const response = await endPointLogin.postLogin(dataRequestLogin.dataLogin);
    expect(response.status).toEqual(200);
    token = response.body.accessToken;
});

describe('Test Case untuk menu category',() => {
    test('get list category',async () => {
        const response = await endPointCategory.getListCategoryNews(dataRequestCategory.paramGetCategory)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schemaCategory.schemaListCategory);
    });

    test('get detail category', async () => {
        const response = await endPointCategory.getDetailCategoryNews(dataRequestCategory.idCategory)
        .set('Authorization', `Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schemaCategory.schemaDetailCategory);
    });

    test('create category', async ()=> {
        const response = await endPointCategory.createCategoryNews(dataRequestCategory.createCategoryNews)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(201);
        expect(response.body).toMatchSchema(schemaCategory.schemaCreateCategory);
    });

    test('get id category news', async ()=> {
        const response = await endPointCategory.getListCategoryNews(dataRequestCategory.paramUpdateCategory)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        idCategoryNews = response.body.data[0]._id;
    });

    test('update category news', async () => {
        const response = await endPointCategory.updateCategoryNews(idCategoryNews,dataRequestCategory.updateCategoryNews)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
        expect(response.body).toMatchSchema(schemaCategory.schemaCreateCategory);
        expect(response.body.name).toBe(dataRequestCategory.updateCategoryNews.name);
        expect(response.body.route).toBe(dataRequestCategory.updateCategoryNews.route);
    });

    test('delete category news', async () => {
        const response = await endPointCategory.deleteCategoryNews(idCategoryNews)
        .set('Authorization',`Bearer ${token}`);
        expect(response.status).toEqual(200);
    });
});