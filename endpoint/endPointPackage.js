const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);

const getListPackage = (paramGetPackage) => api.get(`/admin/package-info/list?take=${paramGetPackage.take}&skip=${paramGetPackage.skip}&keyword=${paramGetPackage.keyword}`);

const getDetailPackage = (id) => api.get(`/admin/package-info/${id}`);

const createPackage = (body) => {
    return api.post('/admin/package-info/create')
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updatePackage = (id,body) => {
    return api.put(`/admin/package-info/${id}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const deletePackage = (id) =>{
    return api.delete(`/admin/package-info/${id}`)
;}

module.exports = {
    getListPackage, getDetailPackage, createPackage, updatePackage, deletePackage
}