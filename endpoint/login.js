const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.authAPI);

const postLogin = (body) => {
    return api.post('/login-web')
    .set('Content-type','application/json')
    .set('Accept','application.json')
    .send(body)
};

module.exports = {
    postLogin
}