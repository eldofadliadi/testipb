const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);


const newWebinar = (body) => {
    return api.post(`/admin/webinar/create`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updateWebinar = (idWebinar,bodyUpdate) => {
    return api.put(`/admin/webinar/${idWebinar}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(bodyUpdate)
};

module.exports = {
    newWebinar, updateWebinar
};