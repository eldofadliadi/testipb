const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);

const getListRegionFaskses = (paramRegion) => api.get(`/admin/medical-facility-region/list?take=${paramRegion.take}&skip=${paramRegion.skip}&keyword=${paramRegion.keyword}`);

const getDetailRegionFaskes = (idRegion) => api.get(`/admin/medical-facility-region/${idRegion}`);

const createRegionFaskes =  (body) => {
    return api.post(`/admin/medical-facility-region/create`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updateRegionFaskes =  (idRegion,body) => {
    return api.put(`/admin/medical-facility-region/${idRegion}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const deleteRegionFaskes = (idRegion) => api.delete(`/admin/medical-facility-region/${idRegion}`);

const getListLokasiFaskes = (paramLokasi) => api.get(`/admin/medical-facility/list?take=${paramLokasi.take}&skip=${paramLokasi.skip}&keyword=${paramLokasi.keyword}`);

const getDetailLokasiFaskes = (idLokasi) => api.get(`/admin/medical-facility/${idLokasi}`);

const createLokasiFaskes = (body) => {
    return api.post(`/admin/medical-facility/create`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updateLokasiFaskes = (idLokasi,body) => {
    return api.put(`/admin/medical-facility/${idLokasi}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const deleteLokasiFaskes = (idLokasi) => api.delete(`/admin/medical-facility/${idLokasi}`);

module.exports = {
    getListRegionFaskses, getDetailRegionFaskes, createRegionFaskes, updateRegionFaskes, deleteRegionFaskes, getDetailLokasiFaskes, getListLokasiFaskes, createLokasiFaskes, updateLokasiFaskes, deleteLokasiFaskes
};