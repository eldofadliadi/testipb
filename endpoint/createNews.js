const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);

const createNews = (body) => {
    return api.post('/admin/news/create')
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const createNews2 = (idCategory,body) => {
    return api.post('/admin/news/create')
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(idCategory,body)
};

const updateNews = (id,body) => {
    return api.put(`/admin/news/${id}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const deleteNews = (id) => {
    return api.delete(`/admin/news/${id}`)
};

module.exports = {
    createNews,updateNews,deleteNews,createNews2
};
