const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.baseurlmain);

const postOrder = (body) => {
    return api.post(`/v1/orders/batch`)
    .set('Accept-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const getDetailOrder = (idOrder) => api.get(`/v1/orders/${idOrder}`);

const getorderList = (paramOrderList) => {
    return api.post(`/v1/orders?take=${paramOrderList.take}&skip=${paramOrderList.skip}`)
};

const updateOrder = (idOrder,body) => {
    return api.put(`/v1/orders/${idOrder}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updateToShip = (body) => {
    return api.put(`/v1/orders/batch`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const cancelOrder = (body) => {
    return api.delete(`/v1/orders/batch`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const postOrderDashboard = (paramOrderDashboard) => {
    return api.post(`/v1/orders/dashboard?startDate=${paramOrderDashboard.startDate}&endDate=${paramOrderDashboard.endDate}`)
};

const postCityDestination = (paramCityDestination) => {
    return api.post(`/v1/orders/city-destination?take=${paramCityDestination.take}&skip=${paramCityDestination.skip}&status=${paramCityDestination.status}&startDate=${paramCityDestination.startDate}&endDate=${paramCityDestination.endDate}`)
};

module.exports = {
    postOrder, getorderList, updateOrder, getDetailOrder, updateToShip, cancelOrder, postOrderDashboard, postCityDestination
}