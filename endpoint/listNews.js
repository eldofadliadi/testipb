const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);

const getListNews = (paramGetListNews) => api.get(`/admin/news/list?take=${paramGetListNews.take}&skip=${paramGetListNews.skip}`);
const detailNews = (idnews) => api.get(`/admin/news/${idnews}`)

module.exports = {
    getListNews, detailNews
}
