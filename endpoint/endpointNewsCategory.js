const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);

const getListCategoryNews = (paramGetCategory) => api.get(`/admin/news-category/list?take=${paramGetCategory.take}&skip=${paramGetCategory.skip}&keyword=${paramGetCategory.keyword}`);

const getDetailCategoryNews = (id) => api.get(`/admin/news-category/${id}`);

const createCategoryNews = (body) => {
    return api.post('/admin/news-category/create')
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updateCategoryNews = (id,body) => {
    return api.put(`/admin/news-category/${id}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const deleteCategoryNews = (id) => {
    return api.delete(`/admin/news-category/${id}`)
};

module.exports = {
    getListCategoryNews, getDetailCategoryNews, createCategoryNews, updateCategoryNews, deleteCategoryNews
}