const request = require('supertest');
const env = require('dotenv').config();
const api = request(process.env.mainAPI);

const getListEmbed = (paramGetListEmbed) => api.get(`/admin/media-embed/list?keyword=${paramGetListEmbed.keyword}&platform=${paramGetListEmbed.platform}&skip=${paramGetListEmbed.skip}&take=${paramGetListEmbed.take}`);

const getDetailEmbed = (id) => api.get(`/admin/media-embed/${id}`);

const createMediaEmbded = (body) => {
    return api.post(`/admin/media-embed/create`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const updateMediaEmbed = (id,body) => {
    return api.put(`/admin/media-embed/${id}`)
    .set('Content-Type','application/json')
    .set('Accept','application/json')
    .send(body)
};

const deleteMediaEmbed = (id) => api.delete(`/admin/media-embed/${id}`);

module.exports = {
    getListEmbed, getDetailEmbed, createMediaEmbded, updateMediaEmbed, deleteMediaEmbed
}