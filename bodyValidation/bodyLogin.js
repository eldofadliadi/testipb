// const bodyRoleAccess = {
//     "roleAccess": [
//         "user.create",
//         "user.read",
//         "user.update",
//         "user.delete",
//         "role.create",
//         "role.read",
//         "role.update",
//         "role.delete",
//         "news.create",
//         "news.read",
//         "news.update",
//         "news.delete",
//         "newsCategory.create",
//         "newsCategory.read",
//         "newsCategory.update",
//         "newsCategory.delete",
//         "bannerVideo.update",
//         "webinar.create",
//         "webinar.read",
//         "webinar.update",
//         "webinar.delete",
//         "packageInfo.create",
//         "packageInfo.read",
//         "packageInfo.update",
//         "packageInfo.delete",
//         "medicalFacility.create",
//         "medicalFacility.read",
//         "medicalFacility.update",
//         "medicalFacility.delete",
//         "medicalFacilityRegion.create",
//         "medicalFacilityRegion.read",
//         "medicalFacilityRegion.update",
//         "medicalFacilityRegion.delete",
//         "oxygen.read",
//         "oxygen.update",
//         "mediaEmbed.create",
//         "mediaEmbed.read",
//         "mediaEmbed.update",
//         "mediaEmbed.delete",
//         "scholarshipApplicant.update",
//         "scholarshipApplicant.read",
//         "scholarshipPayment.update",
//         "scholarshipPayment.read",
//         "scholarshipPayment.create",
//         "scholarshipPayment.delete",
//         "scholarshipTier.create",
//         "scholarshipTier.read",
//         "scholarshipTier.delete",
//         "scholarshipTier.update"
//       ],
// };

// const bodyRole = {
//         "_id": "60ebed4a127d0700116f596b",
//         "username": "superadmin",
//         "password": "$2b$10$l/hYkIAs1ySeq7Wrc.2E4uNOQl5BLxCBOm4Wt1siZPAENsvqt3sOu",
//         "email": "superadmin@sicepat.com",
//         "firstName": "Super",
//         "lastName": "Admin",
//         "roleId": "60ebed49127d0700116f5969",
//         "createdAt": "2021-07-12T07:20:42.041Z",
//         "updatedAt": "2021-07-12T07:20:42.041Z",
//         "__v": 0,
//         "role": {
//             "__v": 0,
//             "_id": "60ebed49127d0700116f5969",
//             "roleName": "Super Admin",
//             "roleAccess": {
//             "_id": "61401adf8fcbf658dcf78bcc",
//             "user": {
//               "_id": "61401adf8fcbf658dcf78bbe",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "role": {
//               "_id": "61401adf8fcbf658dcf78bbf",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "news": {
//               "_id": "61401adf8fcbf658dcf78bc0",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "newsCategory": {
//               "_id": "61401adf8fcbf658dcf78bc1",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "bannerVideo": {
//               "_id": "61401adf8fcbf658dcf78bc2",
//               "update": true
//             },
//             "webinar": {
//               "_id": "61401adf8fcbf658dcf78bc3",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "packageInfo": {
//               "_id": "61401adf8fcbf658dcf78bc4",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "medicalFacility": {
//               "_id": "61401adf8fcbf658dcf78bc5",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "medicalFacilityRegion": {
//               "_id": "61401adf8fcbf658dcf78bc6",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "oxygen": {
//               "_id": "61401adf8fcbf658dcf78bc7",
//               "read": true,
//               "update": true
//             },
//             "mediaEmbed": {
//               "_id": "61401adf8fcbf658dcf78bc8",
//               "create": true,
//               "read": true,
//               "update": true,
//               "delete": true
//             },
//             "scholarshipApplicant": {
//               "_id": "61401adf8fcbf658dcf78bc9",
//               "update": true,
//               "read": true
//             },
//             "scholarshipPayment": {
//               "_id": "61401adf8fcbf658dcf78bca",
//               "update": true,
//               "read": true,
//               "create": true,
//               "delete": true
//             },
//             "scholarshipTier": {
//               "_id": "61401adf8fcbf658dcf78bcb",
//               "create": true,
//               "read": true,
//               "delete": true,
//               "update": true
//             },
//         }
//     }
// };


// function setBody (token,tokenexp){
//   const bodyLoginValid = {
//     "accessToken": token,
//      "accessTokenExpiresAt": tokenexp,
//      "refreshToken": "fe077fd37a3cff47ff6ec92bd6ec1af4e5f566b7",
//      "refreshTokenExpiresAt": "2021-12-10T15:25:50.282Z",
//      "client": {
//        "id": "60ec2cc85f2aa900111e207f",
//        "clientId": "indonesiapastibisa-internal",
//        "clientSecret": "S!C3P4T",
//        "grants": [
//          "password",
//          "refresh_token",
//         "authorization_code"
//         ]
//       },
//       "user": {
//       "_id": "60ebed4a127d0700116f596b",
//       "username": "superadmin",
//       "password": "$2b$10$l/hYkIAs1ySeq7Wrc.2E4uNOQl5BLxCBOm4Wt1siZPAENsvqt3sOu",
//       "email": "superadmin@sicepat.com",
//       "firstName": "Super",
//       "lastName": "Admin",
//       "roleId": "60ebed49127d0700116f5969",
//       "createdAt": "2021-07-12T07:20:42.041Z",
//       "updatedAt": "2021-07-12T07:20:42.041Z",
//       "__v": 0,
//       "role": {
//       "_id": "60ebed49127d0700116f5969",
//       "roleName": "Super Admin",
//       "roleAccess": {
//         "_id": "61401adf8fcbf658dcf78bcc",
//         "user": {
//           "_id": "61401adf8fcbf658dcf78bbe",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "role": {
//           "_id": "61401adf8fcbf658dcf78bbf",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "news": {
//           "_id": "61401adf8fcbf658dcf78bc0",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "newsCategory": {
//           "_id": "61401adf8fcbf658dcf78bc1",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "bannerVideo": {
//           "_id": "61401adf8fcbf658dcf78bc2",
//           "update": true
//         },
//         "webinar": {
//           "_id": "61401adf8fcbf658dcf78bc3",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "packageInfo": {
//           "_id": "61401adf8fcbf658dcf78bc4",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "medicalFacility": {
//           "_id": "61401adf8fcbf658dcf78bc5",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "medicalFacilityRegion": {
//           "_id": "61401adf8fcbf658dcf78bc6",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "oxygen": {
//           "_id": "61401adf8fcbf658dcf78bc7",
//           "read": true,
//           "update": true
//         },
//         "mediaEmbed": {
//           "_id": "61401adf8fcbf658dcf78bc8",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "scholarshipApplicant": {
//           "_id": "61401adf8fcbf658dcf78bc9",
//           "update": true,
//           "read": true
//         },
//         "scholarshipPayment": {
//           "_id": "61401adf8fcbf658dcf78bca",
//           "update": true,
//           "read": true,
//           "create": true,
//           "delete": true
//         },
//         "scholarshipTier": {
//           "_id": "61401adf8fcbf658dcf78bcb",
//           "create": true,
//           "read": true,
//           "delete": true,
//           "update": true
//         }
//       },
//       "__v": 0
//     }
//   },
//   "roleAccess": [
//     "user.create",
//     "user.read",
//     "user.update",
//     "user.delete",
//     "role.create",
//     "role.read",
//     "role.update",
//     "role.delete",
//     "news.create",
//     "news.read",
//     "news.update",
//     "news.delete",
//     "newsCategory.create",
//     "newsCategory.read",
//     "newsCategory.update",
//     "newsCategory.delete",
//     "bannerVideo.update",
//     "webinar.create",
//     "webinar.read",
//     "webinar.update",
//     "webinar.delete",
//     "packageInfo.create",
//     "packageInfo.read",
//     "packageInfo.update",
//     "packageInfo.delete",
//     "medicalFacility.create",
//     "medicalFacility.read",
//     "medicalFacility.update",
//     "medicalFacility.delete",
//     "medicalFacilityRegion.create",
//     "medicalFacilityRegion.read",
//     "medicalFacilityRegion.update",
//     "medicalFacilityRegion.delete",
//     "oxygen.read",
//     "oxygen.update",
//     "mediaEmbed.create",
//     "mediaEmbed.read",
//     "mediaEmbed.update",
//     "mediaEmbed.delete",
//     "scholarshipApplicant.update",
//     "scholarshipApplicant.read",
//     "scholarshipPayment.update",
//     "scholarshipPayment.read",
//     "scholarshipPayment.create",
//     "scholarshipPayment.delete",
//     "scholarshipTier.create",
//     "scholarshipTier.read",
//     "scholarshipTier.delete",
//     "scholarshipTier.update"
//   ],
//   "access_token": "334c3665b7f9d390af82e9ce4a605ac03b74f1b5"
// };
// return bodyLoginValid;
// }


// const bodyLoginValid = {
//     "accessToken": "334c3665b7f9d390af82e9ce4a605ac03b74f1b5",
//      "accessTokenExpiresAt": "2021-12-10T15:25:50.282Z",
//      "refreshToken": "fe077fd37a3cff47ff6ec92bd6ec1af4e5f566b7",
//      "refreshTokenExpiresAt": "2021-12-10T15:25:50.282Z",
//      "client": {
//        "id": "60ec2cc85f2aa900111e207f",
//        "clientId": "indonesiapastibisa-internal",
//        "clientSecret": "S!C3P4T",
//        "grants": [
//          "password",
//          "refresh_token",
//         "authorization_code"
//         ]
//       },
//       "user": {
//       "_id": "60ebed4a127d0700116f596b",
//       "username": "superadmin",
//       "password": "$2b$10$l/hYkIAs1ySeq7Wrc.2E4uNOQl5BLxCBOm4Wt1siZPAENsvqt3sOu",
//       "email": "superadmin@sicepat.com",
//       "firstName": "Super",
//       "lastName": "Admin",
//       "roleId": "60ebed49127d0700116f5969",
//       "createdAt": "2021-07-12T07:20:42.041Z",
//       "updatedAt": "2021-07-12T07:20:42.041Z",
//       "__v": 0,
//       "role": {
//       "_id": "60ebed49127d0700116f5969",
//       "roleName": "Super Admin",
//       "roleAccess": {
//         "_id": "61401adf8fcbf658dcf78bcc",
//         "user": {
//           "_id": "61401adf8fcbf658dcf78bbe",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "role": {
//           "_id": "61401adf8fcbf658dcf78bbf",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "news": {
//           "_id": "61401adf8fcbf658dcf78bc0",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "newsCategory": {
//           "_id": "61401adf8fcbf658dcf78bc1",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "bannerVideo": {
//           "_id": "61401adf8fcbf658dcf78bc2",
//           "update": true
//         },
//         "webinar": {
//           "_id": "61401adf8fcbf658dcf78bc3",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "packageInfo": {
//           "_id": "61401adf8fcbf658dcf78bc4",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "medicalFacility": {
//           "_id": "61401adf8fcbf658dcf78bc5",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "medicalFacilityRegion": {
//           "_id": "61401adf8fcbf658dcf78bc6",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "oxygen": {
//           "_id": "61401adf8fcbf658dcf78bc7",
//           "read": true,
//           "update": true
//         },
//         "mediaEmbed": {
//           "_id": "61401adf8fcbf658dcf78bc8",
//           "create": true,
//           "read": true,
//           "update": true,
//           "delete": true
//         },
//         "scholarshipApplicant": {
//           "_id": "61401adf8fcbf658dcf78bc9",
//           "update": true,
//           "read": true
//         },
//         "scholarshipPayment": {
//           "_id": "61401adf8fcbf658dcf78bca",
//           "update": true,
//           "read": true,
//           "create": true,
//           "delete": true
//         },
//         "scholarshipTier": {
//           "_id": "61401adf8fcbf658dcf78bcb",
//           "create": true,
//           "read": true,
//           "delete": true,
//           "update": true
//         }
//       },
//       "__v": 0
//     }
//   },
//   "roleAccess": [
//     "user.create",
//     "user.read",
//     "user.update",
//     "user.delete",
//     "role.create",
//     "role.read",
//     "role.update",
//     "role.delete",
//     "news.create",
//     "news.read",
//     "news.update",
//     "news.delete",
//     "newsCategory.create",
//     "newsCategory.read",
//     "newsCategory.update",
//     "newsCategory.delete",
//     "bannerVideo.update",
//     "webinar.create",
//     "webinar.read",
//     "webinar.update",
//     "webinar.delete",
//     "packageInfo.create",
//     "packageInfo.read",
//     "packageInfo.update",
//     "packageInfo.delete",
//     "medicalFacility.create",
//     "medicalFacility.read",
//     "medicalFacility.update",
//     "medicalFacility.delete",
//     "medicalFacilityRegion.create",
//     "medicalFacilityRegion.read",
//     "medicalFacilityRegion.update",
//     "medicalFacilityRegion.delete",
//     "oxygen.read",
//     "oxygen.update",
//     "mediaEmbed.create",
//     "mediaEmbed.read",
//     "mediaEmbed.update",
//     "mediaEmbed.delete",
//     "scholarshipApplicant.update",
//     "scholarshipApplicant.read",
//     "scholarshipPayment.update",
//     "scholarshipPayment.read",
//     "scholarshipPayment.create",
//     "scholarshipPayment.delete",
//     "scholarshipTier.create",
//     "scholarshipTier.read",
//     "scholarshipTier.delete",
//     "scholarshipTier.update"
//   ],
//   "access_token": "334c3665b7f9d390af82e9ce4a605ac03b74f1b5"
// };

// module.exports = {
//     bodyLoginValid, bodyRoleAccess, bodyRole , setBody
// }