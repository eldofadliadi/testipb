const locationNotCovered = "Alamat Pengirim [Kecamatan JAGAKARSA, KOTA ADM. JAKARTA SELATAN] tidak tercover untuk kurir jne, silahkan hubungi support kami";

const payloadEmpty = "Alamat Pengirim [Kecamatan  ,  ] tidak tercover untuk kurir  , silahkan hubungi support kami";

const emptyData = "Cannot read property 'length' of undefined";

module.exports = {
    locationNotCovered, payloadEmpty, emptyData
}